package com.example.testproject


import android.app.AlertDialog
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SimpleItemAnimator
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.archit.calendardaterangepicker.customviews.DateRangeCalendarView
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_detail_balance.view.*
import kotlinx.android.synthetic.main.fragment_detail_balance_dialog.view.*
import kotlinx.android.synthetic.main.fragment_detail_balance_list.view.*
import kotlinx.android.synthetic.main.fragment_transaction_list.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class DetailBalanceFragment : Fragment() {
    var buildercustom : AlertDialog.Builder? = null
    var alertcustom : AlertDialog? = null
    var datetext : String? = null
    var starttext : String? = null
    var endtext : String? = null
    var address = "https://armysploit.tk/api/v1/balance/history?"
    var url : String? = null
    private var requestQueue : RequestQueue? = null
    val response : JSONArray? = null
    private var desctext : String? = null
    private var amounttext : String? = null
    private var totaltext : String? = null
    private var statuspaytext : String? = null
    var adapter = GroupAdapter<ViewHolder>()
    var mRecyclerView : RecyclerView? = null
    var notedetail : TextView? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater!!.inflate(R.layout.fragment_detail_balance, container, false)

        view.date_detail_balance.setOnClickListener{
            dialogdate()
        }
        view.search_detail_balance.setOnClickListener {
            if (datetext != null) {
//                val startdate = datetext!!.substringBefore("-","-")
//                val enddate = datetext!!.substringAfter("-","-")
                url = address+"start_date="+starttext+"&end_date="+endtext

                fetchDetailBalance()

            } else {
                view.date_detail_balance.error = "Required this field"
            }

        }

        mRecyclerView = view.findViewById(R.id.recycler_detail_balance)
        notedetail = view.findViewById(R.id.note_detail_balance)
        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())


        val layoutManager = LinearLayoutManager(getActivity())
        (mRecyclerView!!.getItemAnimator() as SimpleItemAnimator).setSupportsChangeAnimations(false)
        mRecyclerView!!.setLayoutManager(layoutManager)
        mRecyclerView!!.addItemDecoration(DividerItemDecoration(mRecyclerView!!.getContext(), DividerItemDecoration.VERTICAL))

        requestQueue = RequestQueue(cache, network).apply {
            start()
        }

        //recycler_transaction.layoutManager = LinearLayoutManager(activity)

        // Inflate the layout for this fragment
        return view
    }

    private fun fetchDetailBalance() {
        Log.d("URL", url)
        val jsonObjectRequest = object : JsonObjectRequest(
            Request.Method.GET, url, null,
            Response.Listener<JSONObject> { response ->

                val data = response.getJSONArray("data")
                if (data.length()>0){
                    adapter.add(DetailBalanceItem("Deskripsi", "Jumlah", "Total"))

                    for(i in 0 until data.length()) {
                        val jsonObject: JSONObject = data.getJSONObject(i)

                        //statuspaytext = jsonObject.getString("keterangan")

                        adapter.add(DetailBalanceItem(desctext, amounttext, totaltext))

                    }

                    mRecyclerView!!.adapter= adapter
                } else {
                    notedetail!!.text = "Tidak ada data yang tersedia"
                }

            },
            Response.ErrorListener {
                Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequest)

    }

    fun dialogdate () {
        buildercustom = AlertDialog.Builder(getActivity())

        val mView : View = layoutInflater.inflate(R.layout.fragment_detail_balance_dialog,null)
        /*
        val monthstart = Calendar.getInstance()
        monthstart.add(Calendar.MONTH,-4)
        val monthend = Calendar.getInstance()
        monthend.add(Calendar.MONTH,+1)
        mView.dialog_detail_date.setVisibleMonthRange(monthstart,monthend)
        */
        mView.dialog_detail_date.setCalendarListener(object : DateRangeCalendarView.CalendarListener {
            override fun onFirstDateSelected(startDate: Calendar) {

            }

            override fun onDateRangeSelected(startDate: Calendar, endDate: Calendar) {

                val end = ""+endDate.get(Calendar.DAY_OF_MONTH)+"/"+(endDate.get(Calendar.MONTH)+1)+"/"+endDate.get(Calendar.YEAR)
                val start = ""+startDate.get(Calendar.DAY_OF_MONTH)+"/"+(startDate.get(Calendar.MONTH)+1)+"/"+startDate.get(Calendar.YEAR)

                var sdf = SimpleDateFormat("dd/MM/yyyy")
                val datestart = sdf.parse(start)
                val dateend = sdf.parse(end)
                sdf = SimpleDateFormat("dd/MM/yyyy")
                starttext = sdf.format(datestart)
                endtext = sdf.format(dateend)

                datetext = starttext+" - "+endtext

            }
        })
        mView.dialog_ok.setOnClickListener{

            view!!.date_detail_balance.setText(datetext)
            alertcustom!!.dismiss()
        }
        mView.dialog_cancel.setOnClickListener{
            alertcustom!!.dismiss()
        }
        buildercustom!!.setView(mView)
        alertcustom = buildercustom!!.create()
        alertcustom!!.show()

    }

}


class DetailBalanceItem(val desc: CharSequence? = "",val amount: CharSequence? = "",val total: CharSequence? = "") : Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {
//        val pos = viewHolder.adapterPosition
//        if (pos == 0 ){
//
//        } else {
//
//        }
        viewHolder.itemView.desc_detail_balance.text = desc
        viewHolder.itemView.amount_detail_balance.text = amount
        viewHolder.itemView.total_detail_balance.text = total
    }
    override fun getLayout(): Int {
        return R.layout.fragment_detail_balance_list
    }

}




