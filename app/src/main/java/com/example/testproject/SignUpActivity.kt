package com.example.testproject

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.OpenableColumns
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.provider.MediaStore
//import sun.font.LayoutPathImpl.getPath
import android.app.Activity
import android.net.Uri
import android.widget.TextView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONObject
import java.io.File
import java.lang.Boolean
import java.util.HashMap


//class SignUpActivity : Fragment() {
//
//    override fun onCreateView(
//        inflater: LayoutInflater, container: ViewGroup?,
//        savedInstanceState: Bundle?)
//
//            : View? {
//        val view: View = inflater!!.inflate(R.layout.activity_sign_up, container, false)
//        val uploadImage = view.findViewById<Button>(R.id.button2)
//
//        uploadImage.setOnClickListener({
//        val intent = Intent()
//            .setType("image/*")
//            .setAction(Intent.ACTION_GET_CONTENT)
//        startActivityForResult(Intent.createChooser(intent,"Select Image File"),111)
//        })
//
//    return view
//    }

//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//
//        if (requestCode == 111 && resultCode == RESULT_OK) {
//            val selectedFile = data?.data //The uri with the location of the file
//        }
//    }

    class SignUpActivity : AppCompatActivity() {
        var imageurl : String? = null
        var uri : Uri? = null
        var file : File? = null
        private var requestQueue : RequestQueue? = null
        var upload: Button? = null
        var daftar: Button? = null
        var namaFile: TextView? = null
        private var bitmap: Bitmap? = null
        private lateinit var imageView: ImageView
//        private val uiHelper = UiHelper()
        private val reference = this

        companion object {
            private const val PICK_IMAGE_REQUEST_CODE = 546
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_sign_up)
            namaFile = findViewById(R.id.NamaFile)
            daftar = findViewById<Button>(R.id.button6)
            daftar!!.setOnClickListener{
                sign_up_dorenmi()
            }

            //VOLLEY REQUEST
            val cache = DiskBasedCache(this.cacheDir, 1024 * 1024) // 1MB cap
            // Set up the network to use HttpURLConnection as the HTTP client.
            val network = BasicNetwork(HurlStack())
            requestQueue = RequestQueue(cache, network).apply {
                start()

            }

            upload = findViewById(R.id.button2)
            upload?.setOnClickListener({
                val intent = Intent(
                    Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                )
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST_CODE)
            })
        }

        private fun sign_up_dorenmi() {

//            val url ="https://armysploit.tk/api/v1/register/personal"
            val url ="https://api.imgur.com/3/upload"
//            params["image"] = uri.toString()
//            params["image"] = file.toString()
            val params = HashMap<String,String>()
            params["image"] = file!!.name
            val jsonObject = JSONObject(params)

            val request = object : JsonObjectRequest(
                Method.POST,url,jsonObject,
                com.android.volley.Response.Listener { response ->

                    Log.d("response", response.toString())
                    // Process the json
                    Log.d("STATUS CODE", response.getInt("status").toString())
                    if (response.getInt("status")==200){
                        val data = response.getJSONObject("data")
                        val link = data.getString("link")

                        Log.d("LINK", link)
//                        var intent = Intent (this,LoginActivity::class.java)
//                        startActivity (intent)
                    } else {
                        Log.d("ERROR SIGN UP", response.getInt("status_code").toString())
                    }

                }, com.android.volley.Response.ErrorListener{
                    // Error in request
                    Log.d("ERROR NETWORK", "TESTING ERROR")
                }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
//                headers.put("authorization","Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEyYjEyYzFlNmEwYmRkOTE1NzQ0ODhjMzExNjFhNzUzYzk1NWZmNWI3ZmNmMTFiMGU0YjY4MzUxMDQ0N2RjOTUwMTVmNDcyYTRkMGFhNDkzIn0.eyJhdWQiOiIxIiwianRpIjoiYTJiMTJjMWU2YTBiZGQ5MTU3NDQ4OGMzMTE2MWE3NTNjOTU1ZmY1YjdmY2YxMWIwZTRiNjgzNTEwNDQ3ZGM5NTAxNWY0NzJhNGQwYWE0OTMiLCJpYXQiOjE1NjE2MjIxNTQsIm5iZiI6MTU2MTYyMjE1NCwiZXhwIjoxNTkzMjQ0NTU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.WqOnIYqsr5oP0wuV7Vw9Rcaa0N1K1T05mbiFz6Lj9M5_Tu8J_nZ-_M2wcY-OZAF0nX6_QPOoiwxYLh3KEXpIGEb2lPWbunLO_pBD29-OeRxIZGcB3bJuHaQqFYKSohqcY0mhgA4BDTxT2nLJt5ibfAaUef2Tbji_2v298ut7NJ1sCGF1EXzA-5Lpa7K1Pgn5iADTK8un5VEZSdNZceB862n417sD06N226GTVOfA1r3FiP88T0ZQbt_fHEJYnPJIDHiPyZBFVkHFEh6HmgVT6gFITdUakegt7uqIJf5NeKu1oeBITZKFZ7Lkk_rzuNYuArePnBHqfoUwZJPT8ohrCFLuk-Dd7wuxXTevRf3pBQJ8NHwW3fPUTWcPKNKgw89S4RLYy7x_H0P5s3NpKgSTrOfGxANkWIJhh6QEKygMpyiNfBfFW6kKAR05xg32xTUC--FKJObG53jkeihpQ9fbfdeR4JT6aUOOCPCTVS5JePfnV6Gum8MRGr6NxoMiWga4T6Fq4rBODcH-T9x5pUc7rTIOx1sRJwxb0A3iK6Ib9i0AG7HUvz9tgk7IgzLd93r1You2TSrPeXSYPGnmPZUCvzYH7UcTTE5Xz-iwgNLSuSGhe7x9eCIeKDzFnfioVFH5z4MJWevVbB9pkeTBkFEmZDtMsnrbJwy2r8qpIVLxsV0")
//                headers["accept"] = "application/json"
//                headers["content-Type"] = "application/json"
                    headers["authorization"] = "Client-ID 32d0d8a7a067d73"
                    return headers
                }
            }


            // Volley request policy, only one time request to avoid duplicate transaction
            request.retryPolicy = DefaultRetryPolicy(
                60000,
                // 0 means no retry
                2, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
                1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            )
            requestQueue!!.add(request)
        }


//        private fun uploadImage(imageBytes: String) {
//            val materialDialog = uiHelper.showAlwaysCircularProgress(this, "Uploading Image")
//            ServiceApi.Factory.getInstance(this)?.uploadImage(imageBytes)
//                ?.enqueue(object : Callback<StatusMessageResponse> {
//                    override fun onFailure(call: Call<StatusMessageResponse>?, t: Throwable?) {
//                        Toast.makeText(reference, "Image uploaded", Toast.LENGTH_SHORT).show()
//                        materialDialog.dismiss()
//                    }
//
//                    override fun onResponse(call: Call<StatusMessageResponse>?, response: Response<StatusMessageResponse>?) {
//                        materialDialog.dismiss()
//                        // Error Occurred during uploading
//                    }
//                })
//        }



        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == PICK_IMAGE_REQUEST_CODE && RESULT_OK == resultCode) {
                Log.d("NAMA",data!!.data.lastPathSegment)
                uri = data.data
                val filePath = getRealPathFromURIPath(uri!!, this@SignUpActivity)
                file = File(filePath)
                namaFile!!.setText(file!!.getName())

//                data?.let {
//
//                     returnUri ->
//                        contentResolver.query(returnUri, null, null, null, null)
//                    }?.use { cursor ->
//                        /*
//                         * Get the column indexes of the data in the Cursor,
//                         * move to the first row in the Cursor, get the data,
//                         * and display it.
//                         */
//                        val nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
//
//                        val sizeIndex = cursor.getColumnIndex(OpenableColumns.SIZE)
//                        cursor.moveToFirst()
//
//                        Log.d("SIZE", sizeIndex.toString())
//                    }



//                    try {
//                        bitmap = uiHelper.decodeUri(this, data.data)
////                        imageView.setImageBitmap(bitmap)
////                        Log.d("a", it.data.toString())
//                        imageurl = uiHelper.getImageUrl(bitmap!!)
//                    } catch (e: Exception) {
//                        if (bitmap != null) {
////                            imageView.setImageBitmap(bitmap)
//                        } else {
//                        }
//
//                    }

            }
        }

        private fun getRealPathFromURIPath(contentURI: Uri, activity: Activity): String {
            val cursor = activity.contentResolver.query(contentURI, null, null, null, null)
            if (cursor == null) {
                return contentURI.getPath()
            } else {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                return cursor.getString(idx)
            }
        }
}
