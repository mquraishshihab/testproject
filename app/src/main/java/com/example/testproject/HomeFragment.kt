package com.example.testproject


import android.app.DatePickerDialog
import java.util.Calendar
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.navigation.findNavController
import com.mindorks.editdrawabletext.DrawablePosition
import com.mindorks.editdrawabletext.EditDrawableText
import com.mindorks.editdrawabletext.onDrawableClickListener
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import java.util.ArrayList


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class HomeFragment : Fragment() {

    var list = ArrayList<String>()
    var kategoriMobil = arrayOf("Semua Jenis", "Sedan", "SUV", "MPV", "Hatchback", "Coupe")
    var kategoriMotor = arrayOf("Semua Jenis","Automatic", "Manual")
    var kategoriAB = arrayOf("Semua Jenis","Excavator","Crane","Truck")



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)

        //Tanggal & Spinner Mobil
        val CekMobil = view.findViewById<Button>(R.id.button13Mob)
        val MulaiTglMobil = view.findViewById<TextView>(R.id.TanggalSewaMob)
        val SelesaiTglMobil = view.findViewById<TextView>(R.id.tglSelesaiMobil)
        val spinnerMobil = view.findViewById<Spinner>(R.id.spinnerMobil)
        val drawable_Mobil = view.findViewById<EditDrawableText>(R.id.KategoriMob)

        //Tanggal & Spinner Motor
        val CekMotor = view.findViewById<Button>(R.id.button13Mot)
        val MulaiTglMotor = view.findViewById<TextView>(R.id.TanggalSewaMot)
        val SelesaiTglMotor = view.findViewById<TextView>(R.id.tglSelesaiMotor)
        val spinnerMotor = view.findViewById<Spinner>(R.id.spinnerMotor)
        val drawable_Motor = view.findViewById<EditDrawableText>(R.id.KategoriMot)

        //Tanggal & Spinner Alat Berat
        val CekAlatBerat = view.findViewById<Button>(R.id.button13AB)
        val MulaiTglAlatBerat = view.findViewById<TextView>(R.id.TanggalSewaAB)
        val SelesaiTglBerat = view.findViewById<TextView>(R.id.tglSelesaiAlatBerat)
        val spinnerAlatBerat = view.findViewById<Spinner>(R.id.spinnerAlatBerat)
        val drawable_AlatBerat = view.findViewById<EditDrawableText>(R.id.KategoriAB)

        //Tanggal Towing
        val CekTowing = view.findViewById<Button>(R.id.button13Tow)
        val MulaiTglTowing = view.findViewById<TextView>(R.id.TanggalSewaTow)
        val SelesaiTglTowing = view.findViewById<TextView>(R.id.tglSelesaiTowing)
        val drawable_Towing = view.findViewById<EditDrawableText>(R.id.KategoriTow)


        val MenuMobil = view.findViewById<ConstraintLayout>(R.id.constraintLayoutMob)
        val MenuMotor = view.findViewById<ConstraintLayout>(R.id.constraintLayoutMot)
        val MenuAlatBerat = view.findViewById<ConstraintLayout>(R.id.constraintLayoutAB)
        val MenuTowing = view.findViewById<ConstraintLayout>(R.id.constraintLayoutTow)

        view.car.setOnClickListener({
            MenuMobil.visibility = View.VISIBLE
            MenuMotor.visibility = View.GONE
            MenuAlatBerat.visibility = View.GONE
            MenuTowing.visibility = View.GONE
        })

        view.bike.setOnClickListener({
            MenuMobil.visibility = View.GONE
            MenuMotor.visibility = View.VISIBLE
            MenuAlatBerat.visibility = View.GONE
            MenuTowing.visibility = View.GONE
        })

        view.train.setOnClickListener({
            MenuMobil.visibility = View.GONE
            MenuMotor.visibility = View.GONE
            MenuAlatBerat.visibility = View.VISIBLE
            MenuTowing.visibility = View.GONE
        })

        view.towing.setOnClickListener({
            MenuMobil.visibility = View.GONE
            MenuMotor.visibility = View.GONE
            MenuAlatBerat.visibility = View.GONE
            MenuTowing.visibility = View.VISIBLE
        })


        //Mobil
        CekMobil.setOnClickListener({
            val bundle = Bundle()
            bundle.putString("query", "bandung")
            bundle.putString("start_date", "27/06/2019")
            //if (enddate not null)  bundle.putString("end_date", transactionItem.harga.toString())


            bundle.putString("qty", "1")
            bundle.putString("category", "car")
            bundle.putString("type", "0")
            view.findNavController().navigate(R.id.action_homeFragment_to_searchFragment, bundle)
        })

        // Tanggal Sewa Mobil
        SelesaiTglMobil.setOnClickListener({
            val c: Calendar = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val bundle = Bundle()
            val dpd =
                DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in Toast
                    Toast.makeText(activity, """$dayOfMonth - ${monthOfYear + 1} - $year""", Toast.LENGTH_LONG).show()
                    SelesaiTglMobil.setText("""$dayOfMonth/${monthOfYear + 1}/$year""")
                }, year, month, day)
            dpd.show()
        })

        MulaiTglMobil.setOnClickListener({
            val c: Calendar = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val bundle = Bundle()
            val dpd =
                DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in Toast
                    Toast.makeText(activity, """$dayOfMonth/${monthOfYear + 1}/$year""", Toast.LENGTH_LONG).show()
                    MulaiTglMobil.setText("""$dayOfMonth/${monthOfYear + 1}/$year""")
                }, year, month, day)
            dpd.show()
        })


        //Spinner Mobil
        // Create an ArrayAdapter using a simple spinnerMobil layout and languages array
        val aa = ArrayAdapter(activity, android.R.layout.simple_spinner_item, kategoriMobil)
        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        spinnerMobil.adapter = aa

        spinnerMobil.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(arg0: AdapterView<*>, arg1: View?, position: Int, id: Long) {
                Log.d("test",position.toString())
            }
            override fun onNothingSelected(arg0: AdapterView<*>) {
            }
        }

        //Jumlah Mobil
        var counter : Int = 0

        drawable_Mobil.setDrawableClickListener(object : onDrawableClickListener {
            override fun onClick(target: DrawablePosition) {
                when (target) {
                    DrawablePosition.RIGHT ->
                        drawable_Mobil.setText("" + ++counter)
                    DrawablePosition.LEFT ->{
                        if(counter >1) {
                            drawable_Mobil.setText("" + --counter)
                        }
                        else{
                            Toast.makeText(activity,"Tidak bisa kurang dari 1", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        })
        //Mobil


        //Motor
        CekMotor.setOnClickListener({
            val bundleMot = Bundle()
            bundleMot.putString("query", "bandung")
            bundleMot.putString("start_date", "27/06/2019")
            //if (enddate not null)  bundle.putString("end_date", transactionItem.harga.toString())

            bundleMot.putString("qty", "1")
            bundleMot.putString("category", "motorcycle")
            bundleMot.putString("type", "0")
            view.findNavController().navigate(R.id.action_homeFragment_to_searchFragment, bundleMot)
        })


        SelesaiTglMotor.setOnClickListener({
            val c: Calendar = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val bundle = Bundle()
            val dpd =
                DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in Toast
                    Toast.makeText(activity, """$dayOfMonth - ${monthOfYear + 1} - $year""", Toast.LENGTH_LONG).show()
                    SelesaiTglMotor.setText("""$dayOfMonth - ${monthOfYear + 1} - $year""")
                }, year, month, day)
            dpd.show()
        })

        MulaiTglMotor.setOnClickListener({
            val c: Calendar = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val bundle = Bundle()
            val dpd =
                DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in Toast
                    Toast.makeText(activity, """$dayOfMonth - ${monthOfYear + 1} - $year""", Toast.LENGTH_LONG).show()
                    MulaiTglMotor.setText("""$dayOfMonth - ${monthOfYear + 1} - $year""")
                }, year, month, day)
            dpd.show()
        })


        //Spinner Motor

        // Create an ArrayAdapter using a simple spinnerMobil layout and languages array
        val bb = ArrayAdapter(activity, android.R.layout.simple_spinner_item, kategoriMotor)
        // Set layout to use when the list of choices appear
        bb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        spinnerMotor.adapter = bb

        spinnerMotor.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(arg0: AdapterView<*>, arg1: View?, position: Int, id: Long) {
//                textView_msg!!.text = "Selected : "+kategoriMobil[position]
            }
            override fun onNothingSelected(arg0: AdapterView<*>) {
            }
        }

        //Jumlah Motor
        var counterMotor : Int = 0

        drawable_Motor.setDrawableClickListener(object : onDrawableClickListener {
            override fun onClick(target: DrawablePosition) {
                when (target) {
                    DrawablePosition.RIGHT ->
                        drawable_Motor.setText("" + ++counterMotor)
                    DrawablePosition.LEFT ->{
                        if(counterMotor >1) {
                            drawable_Motor.setText("" + --counterMotor)
                        }
                        else{
                            Toast.makeText(activity,"Tidak bisa kurang dari 1", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        })
        //Motor


        //Alat Berat
        CekAlatBerat.setOnClickListener({
            val bundleAB = Bundle()
            bundleAB.putString("query", "bandung")
            bundleAB.putString("start_date", "27/06/2019")
            //if (enddate not null)  bundle.putString("end_date", transactionItem.harga.toString())

            bundleAB.putString("qty", "1")
            bundleAB.putString("category", "heavy-equipment")
            bundleAB.putString("type", "0")
            view.findNavController().navigate(R.id.action_homeFragment_to_searchFragment, bundleAB)
        })


        SelesaiTglBerat.setOnClickListener({
            val c: Calendar = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val bundle = Bundle()
            val dpd =
                DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in Toast
                    Toast.makeText(activity, """$dayOfMonth - ${monthOfYear + 1} - $year""", Toast.LENGTH_LONG).show()
                    SelesaiTglBerat.setText("""$dayOfMonth - ${monthOfYear + 1} - $year""")
                }, year, month, day)
            dpd.show()
        })

        MulaiTglAlatBerat.setOnClickListener({
            val c: Calendar = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val bundle = Bundle()
            val dpd =
                DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in Toast
                    Toast.makeText(activity, """$dayOfMonth - ${monthOfYear + 1} - $year""", Toast.LENGTH_LONG).show()
                    MulaiTglAlatBerat.setText("""$dayOfMonth - ${monthOfYear + 1} - $year""")
                }, year, month, day)
            dpd.show()
        })


        //Spinner Alat Berat
        // Create an ArrayAdapter using a simple spinnerMobil layout and languages array
        val cc = ArrayAdapter(activity, android.R.layout.simple_spinner_item, kategoriAB)
        // Set layout to use when the list of choices appear
        cc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        spinnerAlatBerat.adapter = cc

        spinnerAlatBerat.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(arg0: AdapterView<*>, arg1: View?, position: Int, id: Long) {
//                textView_msg!!.text = "Selected : "+kategoriMobil[position]
            }
            override fun onNothingSelected(arg0: AdapterView<*>) {
            }
        }


        //Jumlah Alat Berat
        var counterAB : Int = 0
        drawable_AlatBerat.setDrawableClickListener(object : onDrawableClickListener {
            override fun onClick(target: DrawablePosition) {
                when (target) {
                    DrawablePosition.RIGHT ->
                        drawable_AlatBerat.setText("" + ++counterAB)
                    DrawablePosition.LEFT ->{
                        if(counterAB >1) {
                            drawable_AlatBerat.setText("" + --counterAB)
                        }
                        else{
                            Toast.makeText(activity,"Tidak bisa kurang dari 1", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        })
        //Alat Berat


        //Towing
        CekTowing.setOnClickListener({
            val bundleTow = Bundle()
            bundleTow.putString("from", "bandung")
            bundleTow.putString("to", "jakarta")
            bundleTow.putString("start_date", "27/06/2019")
//            bundleTow.putString("end_date", "27/06/2019")
            //if (enddate not null)  bundle.putString("end_date", transactionItem.harga.toString())

            bundleTow.putString("qty", "1")
            bundleTow.putString("category", "towing")
            view.findNavController().navigate(R.id.action_homeFragment_to_searchFragment, bundleTow)
        })


        SelesaiTglTowing.setOnClickListener({
            val c: Calendar = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val bundle = Bundle()
            val dpd =
                DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in Toast
                    Toast.makeText(activity, """$dayOfMonth - ${monthOfYear + 1} - $year""", Toast.LENGTH_LONG).show()
                    SelesaiTglTowing.setText("""$dayOfMonth - ${monthOfYear + 1} - $year""")
                }, year, month, day)
            dpd.show()
        })

        MulaiTglTowing.setOnClickListener({
            val c: Calendar = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val bundle = Bundle()
            val dpd =
                DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                    // Display Selected date in Toast
                    Toast.makeText(activity, """$dayOfMonth - ${monthOfYear + 1} - $year""", Toast.LENGTH_LONG).show()
                    MulaiTglTowing.setText("""$dayOfMonth - ${monthOfYear + 1} - $year""")
                }, year, month, day)
            dpd.show()
        })

        //Jumlah Towing
        var counterTowing : Int = 0

        drawable_Towing.setDrawableClickListener(object : onDrawableClickListener {
            override fun onClick(target: DrawablePosition) {
                when (target) {
                    DrawablePosition.RIGHT ->
                        drawable_Towing.setText("" + ++counterTowing)
                    DrawablePosition.LEFT ->{
                        if(counterTowing >1) {
                            drawable_Towing.setText("" + --counterTowing)
                        }
                        else{
                            Toast.makeText(activity,"Tidak bisa kurang dari 1", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        })
        //Towing




        return view
    }


}

