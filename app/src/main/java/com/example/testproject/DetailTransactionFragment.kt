package com.example.testproject


import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_detail_history.view.*
import kotlinx.android.synthetic.main.fragment_detail_transaction.*
import kotlinx.android.synthetic.main.fragment_detail_transaction.view.*
import org.json.JSONObject
import java.lang.Boolean.FALSE
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class DetailTransactionFragment : Fragment(), OnMapReadyCallback {

    var adapter = GroupAdapter<ViewHolder>()
    private var requestQueue : RequestQueue? = null
    var addressline : List<Address>? = null
    var geocoder: Geocoder? = null
    var addresstext: String? = null
    var marker: Marker? = null

    override fun onMapReady(p0: GoogleMap?) {
        mGoogleMap = p0!!
        mGoogleMap.uiSettings.setAllGesturesEnabled(FALSE)
    }
    var url_dettrans = "https://armysploit.tk/api/v1/order/"
    lateinit var mapFragment: SupportMapFragment
    lateinit var mGoogleMap: GoogleMap
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater!!.inflate(R.layout.fragment_detail_transaction, container, false)

        mapFragment = childFragmentManager!!.findFragmentById(R.id.map_detail_trans) as SupportMapFragment
        mapFragment.getMapAsync(this)
        geocoder = Geocoder(getActivity())

        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        val network = BasicNetwork(HurlStack())
        requestQueue = RequestQueue(cache, network).apply {
            start()
        }

        if (arguments!!.size() > 0){
            val invoice = arguments?.getString("invoice")
            url_dettrans = url_dettrans+invoice
            fetchDetailTransaction()
        } else {
            view.findNavController().navigateUp()
        }
        view.cancel_detail_trans
        view.conf_detail_trans
        view.return_detail_trans


//        view.img_map_detail.setOnTouchListener(object : View.OnTouchListener{
//            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
//                val action = event!!.action
//                when (action) {
//                     MotionEvent.ACTION_DOWN -> {
//                         view.scrollview_detail_trans.requestDisallowInterceptTouchEvent(true);
//                         return false
//                     }
//                     MotionEvent.ACTION_UP ->{
//                         view.scrollview_detail_trans.requestDisallowInterceptTouchEvent(false);
//                         return true
//                     }
//
//                     MotionEvent.ACTION_MOVE ->
//                     {
//                         view.scrollview_detail_trans.requestDisallowInterceptTouchEvent(true);
//                         return false
//                     }
//                    else ->
//                        return true
//                }
//            }
//        })

        return view
    }

    private fun fetchDetailTransaction() {
        val jsonObjectRequest = object : JsonObjectRequest(
            Method.GET, url_dettrans, null,
            Response.Listener<JSONObject> { response ->

                if (response.getInt("status_code") == 200) {
                    val data = response.getJSONObject("data")

                    val product = data.getJSONObject("product")

                    val invoice_no = product.getString("invoice_no")
                    val image_uri = product.getString("image_uri")
                    val product_name = product.getString("product_name")
                    val charge = product.getInt("charge")
                    val qty = product.getInt("qty")

                    view!!.invoice_detail_trans.text = "Invoice : "+invoice_no
                    Picasso.get().load(image_uri).resize(200,200).into(view!!.image_detail_trans)
                    view!!.item_detail_trans.text = product_name
                    view!!.qty_detail_trans.text = "Jumlah : "+qty.toString()
                    view!!.price_detail_trans.text = "Rp."+charge.toString()

                    val delivery_info = data.getJSONObject("delivery_info")
                        val receipt = delivery_info.getString("receipt")
                        val address_pick = delivery_info.getString("address_pick")
                    view!!.recipient_detail_trans.text = receipt
                    view!!.address_detail_trans.text = address_pick

                    //Google Map Address
                    load_map(address_pick)

                    val payment = data.getJSONObject("payment")
                        val status = payment.getString("status")
                        val payment_date = payment.getString("payment_date")
                        val total_amount = payment.getInt("total_amount")

                    //format date
                    var sdf = SimpleDateFormat("yyyy-MM-dd")
                    val paydate = sdf.parse(payment_date)
                    sdf = SimpleDateFormat("dd MMM yyyy", Locale("in", "ID"))
                    val datepay = sdf.format(paydate)

                    view!!.pay_detail_trans.text = status
                    view!!.date_detail_trans.text = datepay
                    view!!.total_detail_trans.text = "Rp."+total_amount.toString()

                    val details = data.getJSONObject("details")
                        val vendor_name = details.getString("vendor_name")
                        val order_status_code = details.getString("order_status_code")
                        val message = details.getString("message")

                    view!!.vendor_detail_trans.text= vendor_name
                    view!!.vendorname_detail_trans.text= vendor_name
                    view!!.deal_detail_trans.text = message

                } else {

                }

            },
            Response.ErrorListener {
                Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequest)
    }

    private fun load_map(address_pick: String?) {
        addressline = geocoder!!.getFromLocationName(address_pick,1)
        if((addressline as MutableList<Address>?)!!.size > 0) {
            val address = addressline!!.get(0)
            val lat = address!!.latitude
            val lng = address!!.longitude
            val place = LatLng(lat,lng)
            if (marker==null) {
                marker = mGoogleMap.addMarker(MarkerOptions().position(place))
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place,18.0f),1500,null)
            } else {
                mGoogleMap.clear()
                marker = mGoogleMap.addMarker(MarkerOptions().position(place))
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place,18.0f),1000,null)
            }

        } else {
            Toast.makeText(activity, "Alamat tidak dapat ditemukan", Toast.LENGTH_LONG).show()
        }
    }


}
