package com.example.testproject


import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SimpleItemAnimator
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_address.view.*
import kotlinx.android.synthetic.main.fragment_address_list.view.*
import kotlinx.android.synthetic.main.fragment_balance.view.*
import kotlinx.android.synthetic.main.fragment_bank_account_list.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Boolean


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
var adapteraddress = GroupAdapter<ViewHolder>()

class AddressFragment : Fragment() {

    val url = "https://armysploit.tk/api/v1/address/all"
    private var requestQueue : RequestQueue? = null
    val response : JSONArray? = null
    private var addresstext : String? = null
    private var balnumacctext : String? = null
    private var idtext : String? = null
    private var statuspaytext : String? = null
//    private var adapteraddress: GroupAdapter<ViewHolder>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater!!.inflate(R.layout.fragment_address, container, false)

        view.add_address.setOnClickListener{
            view.findNavController().navigate(R.id.action_addressFragment_to_addAddressFragment)
        }
        /*
        view.edit_address.setOnClickListener{
            view.findNavController().navigate(R.id.action_addressFragment_to_editAddressFragment)
        }
        */


        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())
        val mRecyclerView = view.findViewById<RecyclerView>(R.id.recycler_address)
        mRecyclerView!!.isNestedScrollingEnabled = Boolean.FALSE
        val layoutManager = LinearLayoutManager(getActivity())
        (mRecyclerView.getItemAnimator() as SimpleItemAnimator).setSupportsChangeAnimations(false)
        mRecyclerView.setLayoutManager(layoutManager)

        requestQueue = RequestQueue(cache, network).apply {
            start()
        }

        fetchAddress(mRecyclerView)
        //recycler_transaction.layoutManager = LinearLayoutManager(activity)

/*
        mConstraintLayout.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("TEST", "TESTING")
            findNavController().navigate(R.id.action_transactionFragment_to_detailTransactionFragment, bundle)
            //Toast.makeText(activity, "TEXT", Toast.LENGTH_LONG).show()
        }
        */
        // Inflate the layout for this fragment
        return view
    }

    private fun fetchAddress(
        mRecyclerView: RecyclerView
    ) {

        val jsonObjectRequest = object : JsonObjectRequest(
            Request.Method.GET, url, null,
            Response.Listener<JSONObject> { response ->
                val data = response.getJSONArray("data")
                if (data.length() > 0 ){
                    for(i in 0 until data.length()) {
                        val jsonObject: JSONObject = data.getJSONObject(i)

                        val address = jsonObject.getString("address")
                        val address_id = jsonObject.getString("address_id")
                        val lat = jsonObject.getString("lat")
                        val lng = jsonObject.getString("lng")

                        adapteraddress.add(AddressItem(i,address, address_id, lat, lng, context!!))

                    }
                    mRecyclerView.adapter= adapteraddress
                } else {
                    Toast.makeText(context,"Anda belum mendaftarkan alamat..",Toast.LENGTH_LONG).show()
                }

            },
            Response.ErrorListener {
                Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()

//                headers.put("authorization","Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEyYjEyYzFlNmEwYmRkOTE1NzQ0ODhjMzExNjFhNzUzYzk1NWZmNWI3ZmNmMTFiMGU0YjY4MzUxMDQ0N2RjOTUwMTVmNDcyYTRkMGFhNDkzIn0.eyJhdWQiOiIxIiwianRpIjoiYTJiMTJjMWU2YTBiZGQ5MTU3NDQ4OGMzMTE2MWE3NTNjOTU1ZmY1YjdmY2YxMWIwZTRiNjgzNTEwNDQ3ZGM5NTAxNWY0NzJhNGQwYWE0OTMiLCJpYXQiOjE1NjE2MjIxNTQsIm5iZiI6MTU2MTYyMjE1NCwiZXhwIjoxNTkzMjQ0NTU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.WqOnIYqsr5oP0wuV7Vw9Rcaa0N1K1T05mbiFz6Lj9M5_Tu8J_nZ-_M2wcY-OZAF0nX6_QPOoiwxYLh3KEXpIGEb2lPWbunLO_pBD29-OeRxIZGcB3bJuHaQqFYKSohqcY0mhgA4BDTxT2nLJt5ibfAaUef2Tbji_2v298ut7NJ1sCGF1EXzA-5Lpa7K1Pgn5iADTK8un5VEZSdNZceB862n417sD06N226GTVOfA1r3FiP88T0ZQbt_fHEJYnPJIDHiPyZBFVkHFEh6HmgVT6gFITdUakegt7uqIJf5NeKu1oeBITZKFZ7Lkk_rzuNYuArePnBHqfoUwZJPT8ohrCFLuk-Dd7wuxXTevRf3pBQJ8NHwW3fPUTWcPKNKgw89S4RLYy7x_H0P5s3NpKgSTrOfGxANkWIJhh6QEKygMpyiNfBfFW6kKAR05xg32xTUC--FKJObG53jkeihpQ9fbfdeR4JT6aUOOCPCTVS5JePfnV6Gum8MRGr6NxoMiWga4T6Fq4rBODcH-T9x5pUc7rTIOx1sRJwxb0A3iK6Ib9i0AG7HUvz9tgk7IgzLd93r1You2TSrPeXSYPGnmPZUCvzYH7UcTTE5Xz-iwgNLSuSGhe7x9eCIeKDzFnfioVFH5z4MJWevVbB9pkeTBkFEmZDtMsnrbJwy2r8qpIVLxsV0")
//                headers["accept"] = "application/json"
//                headers["content-Type"] = "application/json"
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequest)

    }

    override fun onDestroyView() {
        requestQueue!!.cache.clear()
        adapteraddress.clear()
        super.onDestroyView()
    }
}

class AddressItem(val num : Int,val address: CharSequence? = "", val id: CharSequence? = "", val lat : String, val lng : String, val context: Context) : Item<ViewHolder>(){
    var alertbuilder = AlertDialog.Builder(context)
    var alertDialog : AlertDialog? = null
    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.desc_address.text = address

        viewHolder.itemView.edit_address.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("id", id.toString())
            bundle.putString("address", address.toString())
            bundle.putString("lat", lat)
            bundle.putString("lng", lng)
            it.findNavController().navigate(R.id.action_addressFragment_to_editAddressFragment, bundle)
        }

        viewHolder.itemView.delete_address.setOnClickListener {
            setDialog(id.toString(), position)

        }

    }
    override fun getLayout(): Int {
        return R.layout.fragment_address_list
    }

    private fun setDialog(id: String, position: Int) {
        alertbuilder.setPositiveButton("OK", object : DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                DeleteAddress(id, position)

            }
        })
        alertbuilder.setNegativeButton("Cancel",null)
        alertbuilder.setMessage("Anda yakin ingin menghapus alamat ini?").setTitle("Alamat")
        alertDialog = alertbuilder.create()
        alertDialog!!.show()
    }

    private fun DeleteAddress(id: String, position: Int) {
        val cache = DiskBasedCache(context.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())
        val requestQueue = RequestQueue(cache, network).apply {
            start()
        }
        val url_delete = "https://armysploit.tk/api/v1/address/"+id+"/delete"
        Log.d("URL", url_delete)
        val jsonObjectRequest = object : JsonObjectRequest(
            Method.DELETE, url_delete, null,
            Response.Listener<JSONObject> { response ->
                Log.d("RESPONSE",response.toString())
                adapteraddress.remove(adapteraddress.getItem(position))
                //adapteraddress.notifyItemChanged(position)
                adapteraddress.notifyDataSetChanged()
//                mRecyclerView.adapter= adapteraccount
                requestQueue.stop()
            },
            Response.ErrorListener {
                Toast.makeText(context, "Error", Toast.LENGTH_LONG).show()
                requestQueue.stop()
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()

//                headers.put("authorization","Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEyYjEyYzFlNmEwYmRkOTE1NzQ0ODhjMzExNjFhNzUzYzk1NWZmNWI3ZmNmMTFiMGU0YjY4MzUxMDQ0N2RjOTUwMTVmNDcyYTRkMGFhNDkzIn0.eyJhdWQiOiIxIiwianRpIjoiYTJiMTJjMWU2YTBiZGQ5MTU3NDQ4OGMzMTE2MWE3NTNjOTU1ZmY1YjdmY2YxMWIwZTRiNjgzNTEwNDQ3ZGM5NTAxNWY0NzJhNGQwYWE0OTMiLCJpYXQiOjE1NjE2MjIxNTQsIm5iZiI6MTU2MTYyMjE1NCwiZXhwIjoxNTkzMjQ0NTU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.WqOnIYqsr5oP0wuV7Vw9Rcaa0N1K1T05mbiFz6Lj9M5_Tu8J_nZ-_M2wcY-OZAF0nX6_QPOoiwxYLh3KEXpIGEb2lPWbunLO_pBD29-OeRxIZGcB3bJuHaQqFYKSohqcY0mhgA4BDTxT2nLJt5ibfAaUef2Tbji_2v298ut7NJ1sCGF1EXzA-5Lpa7K1Pgn5iADTK8un5VEZSdNZceB862n417sD06N226GTVOfA1r3FiP88T0ZQbt_fHEJYnPJIDHiPyZBFVkHFEh6HmgVT6gFITdUakegt7uqIJf5NeKu1oeBITZKFZ7Lkk_rzuNYuArePnBHqfoUwZJPT8ohrCFLuk-Dd7wuxXTevRf3pBQJ8NHwW3fPUTWcPKNKgw89S4RLYy7x_H0P5s3NpKgSTrOfGxANkWIJhh6QEKygMpyiNfBfFW6kKAR05xg32xTUC--FKJObG53jkeihpQ9fbfdeR4JT6aUOOCPCTVS5JePfnV6Gum8MRGr6NxoMiWga4T6Fq4rBODcH-T9x5pUc7rTIOx1sRJwxb0A3iK6Ib9i0AG7HUvz9tgk7IgzLd93r1You2TSrPeXSYPGnmPZUCvzYH7UcTTE5Xz-iwgNLSuSGhe7x9eCIeKDzFnfioVFH5z4MJWevVbB9pkeTBkFEmZDtMsnrbJwy2r8qpIVLxsV0")
//                headers["accept"] = "application/json"
//                headers["content-Type"] = "application/json"
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue.add(jsonObjectRequest)

    }

}
