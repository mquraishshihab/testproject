package com.example.testproject


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.android.volley.toolbox.*
import kotlinx.android.synthetic.main.fragment_email.view.*
import org.json.JSONObject
import com.android.volley.toolbox.HttpHeaderParser
import android.R.attr.data
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.HttpHeaderParser.parseCharset


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class EmailFragment : Fragment() {
    val url_email = "https://armysploit.tk/api/v1/profile/email"
    private var requestQueue : RequestQueue? = null
    var email : String? = null
    var status_code : Int? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_email, container, false)

        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())


        if (arguments!!.size() > 0) {
            email = arguments!!.getString("email")
            view.old_email.setText(email!!)
        } else {
            view.findNavController().navigateUp()
        }

        view.save_email.setOnClickListener {
            if (view.old_email.text.toString() == email) {
                if (view.new_email.text.toString().contains("@") && view.new_email.length() >= 9) {
                    requestQueue = RequestQueue(cache, network).apply {
                        start()
                    }
                    UpdateEmail()
                } else {
                    view.new_email.error = "Email tidak benar"
                }
            }else {
                view.old_email.error = "Email lama anda salah"
            }

        }

        // Inflate the layout for this fragment
        return view
    }

    private fun UpdateEmail() {
        val params = HashMap<String,String>()
        Log.d("EMAIL", view!!.new_email.text.toString())
        params["email"] = view!!.new_email.text.toString()
        val jsonObject = JSONObject(params)

        val request =  object : JsonObjectRequest(
            Method.PUT,url_email,jsonObject,
            Response.Listener { response ->
                // Process the json
                Log.d("STATUS CODE",status_code.toString())

                Log.d("RESPONSE", response.toString())
                if (response.getInt("status_code") == 204){

                    view!!.findNavController().navigateUp()
                } else {
                    //error
                    if (response.has("data")){

                            val data = response.optJSONObject("data")
                            if (data.has("email")){
                                val email_error = data.getJSONArray("email")
                                val error_text = email_error.getString(0)
                                Log.d("ERROR", error_text)
                            }
                    }
                    Log.d("ERROR CODE", response.getInt("status_code").toString())
                }
                requestQueue!!.stop()
            }, Response.ErrorListener{
                // Error in request
                if(status_code == 204){
                    view!!.findNavController().navigateUp()
                } else {
                    Toast.makeText(context,"Email sudah terdaftar",Toast.LENGTH_LONG).show()
                }
//                Log.d("ERROR", it.toString())
                requestQueue!!.stop()
            })
        {
            override fun parseNetworkError(volleyError: VolleyError?): VolleyError {
                status_code = volleyError!!.networkResponse.statusCode
                return super.parseNetworkError(volleyError)
            }
            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject> {
                if (response != null) {
                    status_code = response.statusCode
                }
                return super.parseNetworkResponse(response)
            }
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["content-type"] = "application/json"
                headers["accept"] = "application/json"
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            2, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue!!.add(request)

    }


}
