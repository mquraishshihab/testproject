package com.example.testproject


import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SimpleItemAnimator
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResponse
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_order.view.*
import kotlinx.android.synthetic.main.fragment_order_list.view.*
import org.json.JSONObject
import java.lang.Boolean.FALSE
import java.util.ArrayList
import com.midtrans.sdk.corekit.callback.TransactionFinishedCallback
import com.midtrans.sdk.corekit.core.*
import com.midtrans.sdk.corekit.models.*
import com.midtrans.sdk.corekit.models.snap.TransactionResult
import com.midtrans.sdk.uikit.SdkUIFlowBuilder
import butterknife.BindView
import butterknife.ButterKnife
import com.example.testproject.Constant.CLIENT_KEY
import com.midtrans.sdk.corekit.models.snap.CreditCard
import java.lang.Boolean.FALSE



// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class OrderFragment : Fragment(), OnMapReadyCallback,TransactionFinishedCallback {
    private var requestQueue : RequestQueue? = null


    //MIDTRANS//
    private var TAG : String?  = "transactionresult"

    @BindView(R.id.continue_order)
    var test : Button? = null


    lateinit var mapFragment: SupportMapFragment
    lateinit var mGoogleMap: GoogleMap
    var addressline : List<Address>? = null
    var geocoder: Geocoder? = null
    var addresstext: String? = null
    var marker: Marker? = null
    var locationManager : LocationManager? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var locationRequesthighaccuracy: LocationRequest
    private lateinit var locationRequestbalanced: LocationRequest
    private lateinit var builder: LocationSettingsRequest.Builder
    private lateinit var result: Task<LocationSettingsResponse>
    private lateinit var loc_response: LocationSettingsResponse

    var adapter = GroupAdapter<ViewHolder>()
    var addresslist = ArrayList<String>()
    var addresslist_id = ArrayList<String>()
    var addrestlist_lat = ArrayList<String>()
    var addresslist_lng = ArrayList<String>()
    var cache : DiskBasedCache? = null
    var network : BasicNetwork? = null
    var addresspick : String? = null
    var AddressOrder : TextView? = null

    override fun onMapReady(p0: GoogleMap?) {
        mGoogleMap = p0!!
        mGoogleMap.uiSettings.setAllGesturesEnabled(FALSE)
    }

    var url = "https://armysploit.tk/api/v1/checkout"
//    var url_detail = "https://armysploit.tk/api/v1/checkout/transaction-details"
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_order, container, false)
        // Inflate the layout for this fragment
//        val count = arguments?.size()
//        val name = arguments?.getString("cart")
//
//        Toast.makeText(activity, name, Toast.LENGTH_LONG).show()


            //MID TRANS//
            val creditcardopt = CreditCard()
        creditcardopt.isSaveCard = FALSE
        creditcardopt.isSecure = FALSE
        creditcardopt.bank = BankType.BCA
        creditcardopt.channel = CreditCard.MIGS

        val test =view.findViewById<Button>(R.id.testMidtrans)//
        view.continue_order.setOnClickListener({
            transactionRequester()
            MidtransSDK.getInstance().startPaymentUiFlow(activity)
            //MidtransSDK.getInstance().startPaymentUiFlow(activity, PaymentMethod.CREDIT_CARD)
            //MidtransSDK.getInstance().startPaymentUiFlow(activity, SNAP_TOKEN);
        })

        ButterKnife.bind(activity!!)
        initMid()
            //MID TRANS//




        mapFragment = childFragmentManager.findFragmentById(R.id.map_order) as SupportMapFragment
        mapFragment.getMapAsync(this)
        geocoder = Geocoder(context)

        val layoutManager = LinearLayoutManager(getActivity())
        (view.recycler_order.getItemAnimator() as SimpleItemAnimator).setSupportsChangeAnimations(false)
        view.recycler_order.setLayoutManager(layoutManager)
        view.recycler_order.addItemDecoration(DividerItemDecoration(view.recycler_order.getContext(), DividerItemDecoration.VERTICAL))

        AddressOrder= view.findViewById(R.id.address_order)

        cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        network = BasicNetwork(HurlStack())
        //requestQueue= RequestQueue(cache,network)
        requestQueue = RequestQueue(cache, network).apply {
        start()
        }
        FetchOrderItem()

        view.select_address_order.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(position!=0){
                    addresspick = addresslist.get(position)

                        AddressOrder!!.visibility = View.VISIBLE
                        LoadMap(addresspick!!)
                } else {
                        AddressOrder!!.visibility = View.GONE
                        mGoogleMap.clear()
                        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(-2.051010, 115.878276),3.5f))
                }

            }
        }
        return view
    }

    private fun initMid(){


        SdkUIFlowBuilder.init()

            .setClientKey(CLIENT_KEY) // client_key is mandatory
            .setContext(activity) // context is mandatory
            .setTransactionFinishedCallback (this)
//                    TransactionFinishedCallback() {
//                    @Override
//                    fun onTransactionFinished(result : TransactionResult) {
//                        Log.w(TAG, result.getResponse().getStatusMessage());
//                    }

            // set transaction finish callback (sdk callback)
            //.setMerchantBaseUrl("https://haryonostudio.id/mid/checkout.php/") //set merchant url (required)
            //.setMerchantBaseUrl("https://armysploit.tk/api/v1/checkout/")
//            .setMerchantBaseUrl("https://ac32305d.ngrok.io/api/v1/pay/")
            .setMerchantBaseUrl("https://armysploit.tk/api/v1/pay/")
            .enableLog(BuildConfig.DEBUG) // enable sdk log (optional)
//            .setDefaultText("open_sans_light.ttf")
            // set theme. it will replace theme on snap theme on MAP ( optional)
            .buildSDK()
    }

    override fun onTransactionFinished(transactionResult: TransactionResult) {
        Log.w(TAG, transactionResult.response.statusMessage)
        Log.d("test :",transactionResult.response.toString())
        Log.d("test :",transactionResult.statusMessage)
    }


    private fun transactionRequester() {
        var userDetail: UserDetail? = LocalDataHandler.readObject("user_details", UserDetail::class.java)
        if (userDetail == null) {
            userDetail = UserDetail()
            userDetail.userFullName = "Farrel Ramadhika"
            userDetail.email = "wenawas@gmail.com"
            userDetail.phoneNumber = "08123456789"
            userDetail.userId = "farrel-${System.currentTimeMillis()}"

            val userAddresses = ArrayList<UserAddress>()
            val userAddress = UserAddress()
            userAddress.address = "Jalan Andalas Gang Sebelah No. 1"
            userAddress.city = "Bandung"
            userAddress.addressType = com.midtrans.sdk.corekit.core.Constants.ADDRESS_TYPE_BOTH
            userAddress.zipcode = "12345"
            userAddress.country = "IDN"
            userAddresses.add(userAddress)
            userDetail.userAddresses = userAddresses
            LocalDataHandler.saveObject("user_details", userDetail)
        }

        val transactionRequest = TransactionRequest(System.currentTimeMillis().toString() + "", 35000.0)
        val itemDetails1 = ItemDetails("BP1", 15000.0, 1, "Test Paket 1")
        val itemDetails2 = ItemDetails("BP2", 20000.0, 1, "Test Paket 2")


        val itemDetailsList = ArrayList<ItemDetails>()
        itemDetailsList.add(itemDetails1)
        itemDetailsList.add(itemDetails2)

        transactionRequest.itemDetails = itemDetailsList
        val billinfo = BillInfoModel("TRST","TEST")
        transactionRequest.billInfoModel = billinfo
        Log.d("Billinfo :" ,billinfo.toString())
        MidtransSDK.getInstance().transactionRequest = transactionRequest


    }


    private fun LoadMap(addresspick: String) {
        addressline = geocoder!!.getFromLocationName(addresspick,1)
        if((addressline as MutableList<Address>?)!!.size > 0) {
            val address = addressline!!.get(0)
            val lat = address.latitude
            val lng = address.longitude
            val place = LatLng(lat,lng)
            if (marker==null) {
                marker = mGoogleMap.addMarker(MarkerOptions().position(place))
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place,18.0f),1500,null)
            } else {
                mGoogleMap.clear()
                marker = mGoogleMap.addMarker(MarkerOptions().position(place))
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place,18.0f),1000,null)
            }
            view!!.address_order.text = addresspick

        } else {
            Toast.makeText(activity, "Terjadi kesalahan ketika menampilkan lokasi", Toast.LENGTH_LONG).show()
        }
    }

    private fun FetchOrderItem() {

        val jsonObjectRequest = object : JsonObjectRequest(
            Method.GET, url, null,
            Response.Listener<JSONObject> { response ->
                Log.d("TEST","SUKSES")
                // val jsonArray = response.getJSONArray("data")//
//                val error = response.getString("error")
//                if (error!=null){
//                    Log.d("ERROR",error)
//                }
//                Log.d("PAGE",url)
                Log.d("Response Order",response.toString())
                val data = response.getJSONObject("data")

                val costobj = data.getJSONObject("cost")
                val cost_rent = costobj.getString("cost_rent")
                val shipping_fee = costobj.getString("shipping_fee")
                val total_cost = costobj.getString("total_cost")

                val items = data.getJSONArray("items")

                    for(i in adapter.itemCount until items.length()) {
                        val jsonObject: JSONObject = items.getJSONObject(i)

//                        val id = jsonObject.getString("id")
                        val product_image = jsonObject.getString("product_image")
                        val product_name = jsonObject.getString("product_name")
                        val cost = jsonObject.getInt("cost")
                        val vendor_name = jsonObject.getString("vendor_name")

                        adapter.add(OrderItem(product_image, product_name, cost, vendor_name))

                }
                view!!.recycler_order.adapter = adapter
                view!!.rent_order.text = cost_rent
                view!!.ship_order.text = shipping_fee
                view!!.total_order.text = total_cost
                fetchAddress()
                //val jumlah = response.length()

            },
            Response.ErrorListener {
                Toast.makeText(activity, "Error Order", Toast.LENGTH_LONG).show()

                requestQueue!!.stop()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()

//                headers.put("authorization","Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEyYjEyYzFlNmEwYmRkOTE1NzQ0ODhjMzExNjFhNzUzYzk1NWZmNWI3ZmNmMTFiMGU0YjY4MzUxMDQ0N2RjOTUwMTVmNDcyYTRkMGFhNDkzIn0.eyJhdWQiOiIxIiwianRpIjoiYTJiMTJjMWU2YTBiZGQ5MTU3NDQ4OGMzMTE2MWE3NTNjOTU1ZmY1YjdmY2YxMWIwZTRiNjgzNTEwNDQ3ZGM5NTAxNWY0NzJhNGQwYWE0OTMiLCJpYXQiOjE1NjE2MjIxNTQsIm5iZiI6MTU2MTYyMjE1NCwiZXhwIjoxNTkzMjQ0NTU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.WqOnIYqsr5oP0wuV7Vw9Rcaa0N1K1T05mbiFz6Lj9M5_Tu8J_nZ-_M2wcY-OZAF0nX6_QPOoiwxYLh3KEXpIGEb2lPWbunLO_pBD29-OeRxIZGcB3bJuHaQqFYKSohqcY0mhgA4BDTxT2nLJt5ibfAaUef2Tbji_2v298ut7NJ1sCGF1EXzA-5Lpa7K1Pgn5iADTK8un5VEZSdNZceB862n417sD06N226GTVOfA1r3FiP88T0ZQbt_fHEJYnPJIDHiPyZBFVkHFEh6HmgVT6gFITdUakegt7uqIJf5NeKu1oeBITZKFZ7Lkk_rzuNYuArePnBHqfoUwZJPT8ohrCFLuk-Dd7wuxXTevRf3pBQJ8NHwW3fPUTWcPKNKgw89S4RLYy7x_H0P5s3NpKgSTrOfGxANkWIJhh6QEKygMpyiNfBfFW6kKAR05xg32xTUC--FKJObG53jkeihpQ9fbfdeR4JT6aUOOCPCTVS5JePfnV6Gum8MRGr6NxoMiWga4T6Fq4rBODcH-T9x5pUc7rTIOx1sRJwxb0A3iK6Ib9i0AG7HUvz9tgk7IgzLd93r1You2TSrPeXSYPGnmPZUCvzYH7UcTTE5Xz-iwgNLSuSGhe7x9eCIeKDzFnfioVFH5z4MJWevVbB9pkeTBkFEmZDtMsnrbJwy2r8qpIVLxsV0")
//                headers["accept"] = "application/json"
//                headers["content-Type"] = "application/json"
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequest)
    }

    private fun fetchAddress() {
        val url_address = "https://armysploit.tk/api/v1/address/all"
        val jsonObjectRequest = object : JsonObjectRequest(
            Request.Method.GET, url_address, null,
            Response.Listener<JSONObject> { response ->
                Log.d("Response Address",response.toString())
                val data = response.getJSONArray("data")
                if (data.length() > 0 ){
                    addresslist.add("Pilih Alamat")
                    addresslist_id.add("")
                    addrestlist_lat.add("")
                    addresslist_lng.add("")
                    for(i in 0 until data.length()) {
                        val jsonObject: JSONObject = data.getJSONObject(i)

                        addresslist.add(jsonObject.getString("address"))
                        addresslist_id.add(jsonObject.getString("address_id"))
                        addrestlist_lat.add(jsonObject.getString("lat"))
                        addresslist_lng.add(jsonObject.getString("lng"))

                    }
                    var arrayAdapter = object : ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_dropdown_item, addresslist){

                        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
                            val tv = super.getDropDownView(position, convertView, parent) as TextView

                            // Set the text color of drop down items
                            tv.setTextColor(Color.BLACK)

                            // If this item is selected item
                            if (position == 0) {
                                // Set spinner selected popup item's text color
                                tv.setTextColor(Color.GRAY)
                            }
                            return tv
                        }
                    }
                    view!!.select_address_order.adapter = arrayAdapter

                } else {
                    view!!.select_address_order.isEnabled = FALSE
                }
                requestQueue!!.stop()
            },
            Response.ErrorListener {
                Toast.makeText(activity, "Error Address", Toast.LENGTH_LONG).show()
                requestQueue!!.stop()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()

//                headers.put("authorization","Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEyYjEyYzFlNmEwYmRkOTE1NzQ0ODhjMzExNjFhNzUzYzk1NWZmNWI3ZmNmMTFiMGU0YjY4MzUxMDQ0N2RjOTUwMTVmNDcyYTRkMGFhNDkzIn0.eyJhdWQiOiIxIiwianRpIjoiYTJiMTJjMWU2YTBiZGQ5MTU3NDQ4OGMzMTE2MWE3NTNjOTU1ZmY1YjdmY2YxMWIwZTRiNjgzNTEwNDQ3ZGM5NTAxNWY0NzJhNGQwYWE0OTMiLCJpYXQiOjE1NjE2MjIxNTQsIm5iZiI6MTU2MTYyMjE1NCwiZXhwIjoxNTkzMjQ0NTU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.WqOnIYqsr5oP0wuV7Vw9Rcaa0N1K1T05mbiFz6Lj9M5_Tu8J_nZ-_M2wcY-OZAF0nX6_QPOoiwxYLh3KEXpIGEb2lPWbunLO_pBD29-OeRxIZGcB3bJuHaQqFYKSohqcY0mhgA4BDTxT2nLJt5ibfAaUef2Tbji_2v298ut7NJ1sCGF1EXzA-5Lpa7K1Pgn5iADTK8un5VEZSdNZceB862n417sD06N226GTVOfA1r3FiP88T0ZQbt_fHEJYnPJIDHiPyZBFVkHFEh6HmgVT6gFITdUakegt7uqIJf5NeKu1oeBITZKFZ7Lkk_rzuNYuArePnBHqfoUwZJPT8ohrCFLuk-Dd7wuxXTevRf3pBQJ8NHwW3fPUTWcPKNKgw89S4RLYy7x_H0P5s3NpKgSTrOfGxANkWIJhh6QEKygMpyiNfBfFW6kKAR05xg32xTUC--FKJObG53jkeihpQ9fbfdeR4JT6aUOOCPCTVS5JePfnV6Gum8MRGr6NxoMiWga4T6Fq4rBODcH-T9x5pUc7rTIOx1sRJwxb0A3iK6Ib9i0AG7HUvz9tgk7IgzLd93r1You2TSrPeXSYPGnmPZUCvzYH7UcTTE5Xz-iwgNLSuSGhe7x9eCIeKDzFnfioVFH5z4MJWevVbB9pkeTBkFEmZDtMsnrbJwy2r8qpIVLxsV0")
//                headers["accept"] = "application/json"
//                headers["content-Type"] = "application/json"
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequest)
    }

}

class OrderItem(
    val product_image: String,
   val product_name: String,
    val cost: Int,
    val vendor_name: String
) : Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.item_order.text = product_name
        viewHolder.itemView.vendor_order.text = vendor_name
        viewHolder.itemView.price_order.text = "Rp."+cost
        Picasso.get().load(product_image).resize(200,200).into(viewHolder.itemView.img_order)
    }
    override fun getLayout(): Int {
        return R.layout.fragment_order_list
    }

}
