package com.example.testproject


import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SimpleItemAnimator
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.*
import com.mindorks.editdrawabletext.DrawablePosition
import com.mindorks.editdrawabletext.EditDrawableText
import com.mindorks.editdrawabletext.onDrawableClickListener
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_bank_account.view.*
import kotlinx.android.synthetic.main.fragment_bank_account_list.view.*
import kotlinx.android.synthetic.main.fragment_cart.*
import kotlinx.android.synthetic.main.fragment_cart.view.*
import kotlinx.android.synthetic.main.fragment_cart_list.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Boolean
import java.lang.Boolean.FALSE
import java.lang.Boolean.TRUE


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
var adaptercart = GroupAdapter<ViewHolder>()
/**
 * A simple [Fragment] subclass.
 *
 */
class CartFragment : Fragment() {
    var bundle : Bundle? = null
    val url = "https://armysploit.tk/api/v1/carts"
    private var requestQueue : RequestQueue? = null
    val response : JSONArray? = null
    private var itemtext : String? = null
    private var pricetext : String? = null
    private var etatext : String? = null
    private var totaltext : String? = null

    var cartItem : Item<ViewHolder>? = null
    var cart : CartItem? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_cart, container, false)
        // Inflate the layout for this fragment

        bundle = Bundle()
        view.pay_cart.setOnClickListener{

//            Log.d("TOTAL",adaptercart!!.itemCount.toString())
            var n = 0
            for (i in 0 until adaptercart!!.itemCount) {
                //cart = cartItem!!.get(i)

                val item = adaptercart!!.getItem(i)
                val cart = item as CartItem

                if (cart.check){
                    n=n+1

                    Log.d("DATA-"+n, cart.qty.toString())
                    //bundle!!.putString("DATA-"+i, cart.qty.toString())
                } else {

                }
            }
            if (n>0){

                bundle!!.putString("cart", n.toString())
                view.findNavController().navigate(R.id.action_cartFragment_to_orderFragment, bundle)
            }

        }
        /*
        view.edit_bank_account.setOnClickListener{
            view.findNavController().navigate(R.id.action_bankAccountFragment_to_editBankAccountFragment)
        }
        */

        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())
        val mRecyclerView = view.findViewById<RecyclerView>(R.id.recycler_cart)
        mRecyclerView!!.isNestedScrollingEnabled = FALSE
        val layoutManager = LinearLayoutManager(getActivity())
        (mRecyclerView.getItemAnimator() as SimpleItemAnimator).setSupportsChangeAnimations(false)
        mRecyclerView.setLayoutManager(layoutManager)

        requestQueue = RequestQueue(cache, network).apply {
            start()
        }

        fetchCart(mRecyclerView)
        //recycler_transaction.layoutManager = LinearLayoutManager(activity)

        // Inflate the layout for this fragment
        return view
    }

    private fun fetchCart(
        mRecyclerView: RecyclerView
    ) {

        val jsonObjectRequest = object : JsonObjectRequest(
            Request.Method.GET, url, null,
            Response.Listener<JSONObject> { response ->

                val data = response.getJSONObject("data")
                val items = data.getJSONArray("items")
                for(i in 0 until items.length()) {
                    val jsonObject: JSONObject = items.getJSONObject(i)

                    val product_image = jsonObject.getString("product_image")
                    val product_id = jsonObject.getString("product_id")
                    val product_name = jsonObject.getString("product_name")

                    val durobj = jsonObject.getJSONObject("duration")
                    val start = durobj.getString("start")
                    val end = durobj.getString("end")
                    val duration = start+" -\n "+end
                    val origin_price = jsonObject.getInt("origin_price")
                    val qty = jsonObject.getInt("qty")
                    val total_item_price = jsonObject.getInt("total_item_price")
                    val cart_uniqid = jsonObject.getString("cart_uniqid")
                    val cart_id_combination = jsonObject.getString("cart_id_combination")


                    //val total = totaltext!!.toInt()
                    adaptercart!!.add(CartItem(context!!,FALSE,product_image, product_id, product_name, duration, origin_price, qty, total_item_price, cart_uniqid, cart_id_combination))

                }

                mRecyclerView.adapter= adaptercart
            },
            Response.ErrorListener {
                Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequest)

    }

    override fun onDestroy() {
        requestQueue!!.cache.clear()
        adaptercart.clear()
        super.onDestroy()
    }
    override fun onDestroyView() {
        requestQueue!!.cache.clear()
        adaptercart.clear()
        super.onDestroyView()
    }

}

class CartItem(val context: Context, var check : kotlin.Boolean, val product_image: CharSequence? = "", val product_id: CharSequence? = "",
               var product_name: CharSequence? = "", val duration: CharSequence? = "", val origin_price: Int,
               var qty: Int, var total_item_price: Int, val cart_uniqid: CharSequence? = "", val cart_id_combination: CharSequence? = "")
    : Item<ViewHolder>(){

    var alertbuilder = AlertDialog.Builder(context)
    var alertDialog : AlertDialog? = null

    fun GetCheck() : kotlin.Boolean{
        return check
    }
    fun SetCheck(check: kotlin.Boolean){
        this.check = check
    }
    fun GetCount() : Int{
        return qty
    }
    fun SetCount(qty: Int){
        this.qty = qty
    }
    override fun bind(viewHolder: ViewHolder, position: Int) {

        val e = viewHolder.itemView.findViewById<EditDrawableText>(R.id.qty_cart)

        viewHolder.itemView.item_cart.text = product_name
        viewHolder.itemView.price_cart.text = "Rp."+origin_price.toString()
        viewHolder.itemView.eta_cart.text = duration
        e.setText(qty.toString())

        viewHolder.itemView.delete_cart.setOnClickListener {
            setDialog(cart_id_combination.toString(), position)
        }

        e.setDrawableClickListener(object : onDrawableClickListener{
            override fun onClick(target: DrawablePosition) {
                when (target) {

                    DrawablePosition.RIGHT -> {
                        qty = qty+1
                        e.setText(qty.toString())
                        viewHolder.itemView.total_cart.text = "Rp."+(total_item_price*qty).toString()
                    }
                    DrawablePosition.LEFT -> {
                        if (qty>1) {
                            qty = qty-1
                            e.setText(qty.toString())
                            viewHolder.itemView.total_cart.text = "Rp."+(total_item_price*qty).toString()
                        }
                    }
                }
            }

        })
        viewHolder.itemView.total_cart.text = "Rp."+total_item_price.toString()

//        e.addTextChangedListener(object : TextWatcher {
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, qty: Int) {
//
//
//            }
//            override fun afterTextChanged(s: Editable?) {
//                // Log.d("SEBELUM",s.toString())
//                Log.d("TESTING",s.toString())
//                if (s.toString() == "" || s!!.isEmpty()) {
//
//                    viewHolder.itemView.total_cart.text = (total*1).toString()
//                } else {
//// Log.d("SETELAH","PROSES")
//                    if (s.toString().toInt() > 0){
//                        this@CartItem.qty = s.toString().toInt()
//                        viewHolder.itemView.total_cart.text = (total*qty).toString()
//                    } else {
//
//                        viewHolder.itemView.total_cart.text = (total*1).toString()
//                    }
//                }
//            }
//
//            override fun beforeTextChanged(s: CharSequence?, start: Int, qty: Int, after: Int) {
//
//            }
//
//        }
//        )

        viewHolder.itemView.check_cart.setOnCheckedChangeListener(null)
        viewHolder.itemView.check_cart.isChecked = GetCheck()
        viewHolder.itemView.check_cart.setOnCheckedChangeListener { buttonView, isChecked ->
            SetCheck(isChecked)
        }

    }

    private fun setDialog(id: String, position: Int) {
        alertbuilder.setPositiveButton("OK", object : DialogInterface.OnClickListener{
            override fun onClick(dialog: DialogInterface?, which: Int) {
                DeleteCart(id, position)

            }
        })
        alertbuilder.setNegativeButton("Cancel",null)
        alertbuilder.setMessage("Anda yakin ingin menghapus barang ini?").setTitle("Keranjang")
        alertDialog = alertbuilder.create()
        alertDialog!!.show()
    }

    private fun DeleteCart(id: String, position: Int) {
        val cache = DiskBasedCache(context.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())
        val requestQueue = RequestQueue(cache, network).apply {
            start()
        }
        val url_delete = "https://armysploit.tk/api/v1/carts/"+id
        Log.d("URL", url_delete)
        val jsonObjectRequest = object : JsonObjectRequest(
            Method.DELETE, url_delete, null,
            Response.Listener<JSONObject> { response ->
                Log.d("RESPONSE",response.toString())
                adaptercart.remove(adaptercart.getItem(position))
//                adapteraccount.notifyItemChanged(position)
                adaptercart.notifyDataSetChanged()
                requestQueue.stop()
            },
            Response.ErrorListener {
                Toast.makeText(context, "Error", Toast.LENGTH_LONG).show()
                requestQueue.stop()
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()

//                headers.put("authorization","Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEyYjEyYzFlNmEwYmRkOTE1NzQ0ODhjMzExNjFhNzUzYzk1NWZmNWI3ZmNmMTFiMGU0YjY4MzUxMDQ0N2RjOTUwMTVmNDcyYTRkMGFhNDkzIn0.eyJhdWQiOiIxIiwianRpIjoiYTJiMTJjMWU2YTBiZGQ5MTU3NDQ4OGMzMTE2MWE3NTNjOTU1ZmY1YjdmY2YxMWIwZTRiNjgzNTEwNDQ3ZGM5NTAxNWY0NzJhNGQwYWE0OTMiLCJpYXQiOjE1NjE2MjIxNTQsIm5iZiI6MTU2MTYyMjE1NCwiZXhwIjoxNTkzMjQ0NTU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.WqOnIYqsr5oP0wuV7Vw9Rcaa0N1K1T05mbiFz6Lj9M5_Tu8J_nZ-_M2wcY-OZAF0nX6_QPOoiwxYLh3KEXpIGEb2lPWbunLO_pBD29-OeRxIZGcB3bJuHaQqFYKSohqcY0mhgA4BDTxT2nLJt5ibfAaUef2Tbji_2v298ut7NJ1sCGF1EXzA-5Lpa7K1Pgn5iADTK8un5VEZSdNZceB862n417sD06N226GTVOfA1r3FiP88T0ZQbt_fHEJYnPJIDHiPyZBFVkHFEh6HmgVT6gFITdUakegt7uqIJf5NeKu1oeBITZKFZ7Lkk_rzuNYuArePnBHqfoUwZJPT8ohrCFLuk-Dd7wuxXTevRf3pBQJ8NHwW3fPUTWcPKNKgw89S4RLYy7x_H0P5s3NpKgSTrOfGxANkWIJhh6QEKygMpyiNfBfFW6kKAR05xg32xTUC--FKJObG53jkeihpQ9fbfdeR4JT6aUOOCPCTVS5JePfnV6Gum8MRGr6NxoMiWga4T6Fq4rBODcH-T9x5pUc7rTIOx1sRJwxb0A3iK6Ib9i0AG7HUvz9tgk7IgzLd93r1You2TSrPeXSYPGnmPZUCvzYH7UcTTE5Xz-iwgNLSuSGhe7x9eCIeKDzFnfioVFH5z4MJWevVbB9pkeTBkFEmZDtMsnrbJwy2r8qpIVLxsV0")
//                headers["accept"] = "application/json"
//                headers["content-Type"] = "application/json"
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue.add(jsonObjectRequest)

    }

    override fun getLayout(): Int {
        return R.layout.fragment_cart_list
    }
}
