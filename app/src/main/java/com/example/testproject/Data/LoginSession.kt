package com.example.testproject.Data


import android.content.Context
import android.content.SharedPreferences
import android.R.id.edit
import java.lang.Boolean.FALSE


class LoginSession(val context: Context) {
    var pref_name = "LoginSession"
    //var sharedPreferences: SharedPreferences? = null
    var editor: SharedPreferences.Editor? = null
    var login_status_session ="login_status"
    var email_session ="email"
    var token_session = "token"
    var sharedPreferences = context.getSharedPreferences(pref_name, Context.MODE_PRIVATE)

    fun getEmail(): String {
        return sharedPreferences!!.getString(email_session, "")
    }

    fun setEmail(key: String, email_session: String) {
        editor = sharedPreferences!!.edit()
        editor!!.putString(key, email_session)
        editor!!.commit()
    }

    fun getToken(): String {
        return sharedPreferences!!.getString(token_session, "")
    }

    fun setToken(key: String, nip_session: String) {
        editor = sharedPreferences!!.edit()
        editor!!.putString(key, nip_session)
        editor!!.commit()
    }

    fun getLogin_status(): Boolean? {
        return sharedPreferences!!.getBoolean(login_status_session, false)
    }

    fun setLogin_status(key: String, login_status_session: Boolean?) {
        editor = sharedPreferences!!.edit()
        editor!!.putBoolean(key, login_status_session!!)
        editor!!.commit()
    }
}