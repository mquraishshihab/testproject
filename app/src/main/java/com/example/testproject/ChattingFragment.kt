package com.example.testproject

import android.content.ContentValues.TAG
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.UserDictionary.Words.APP_ID
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SimpleItemAnimator
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.LruCache
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.*
import com.sendbird.android.*
import com.sendbird.desk.android.SendBirdDesk
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_chat_from.view.*
import kotlinx.android.synthetic.main.fragment_chat_to.view.*
import kotlinx.android.synthetic.main.fragment_chatting.view.*
import kotlinx.android.synthetic.main.fragment_transaction_list.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Boolean
import java.lang.Boolean.FALSE
import java.lang.Boolean.TRUE
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val UNIQUE_HANDLER_ID = "dorenmi_chatting"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ChattingFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ChattingFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class ChattingFragment : Fragment() {
    var testadapter = GroupAdapter<ViewHolder>()
    var groupChannelListQuery : GroupChannelListQuery? = null
    val url = "https://presensi-dinas.000webhostapp.com/absensi/user/history.php?nip=121212121212121212"
    private var requestQueue : RequestQueue? = null
    val response : JSONArray? = null
    private var msgtext : String? = null
    private var statustranstext : String? = null
    private var hargatext : String? = null
    private var statuspaytext : String? = null
    private var mConstraintLayout : ConstraintLayout? = null
    var adapter = GroupAdapter<ViewHolder>()
    var userConnect : User.ConnectionStatus? = null
    var channel : GroupChannel? = null

    var channel_url : String? = null

    // TODO: Rename and change types of parameters
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_chatting, container, false)

        SendBird.connect("mqsakarais44223", object : SendBird.ConnectHandler{
            override fun onConnected(p0: User?, p1: SendBirdException?) {

                //val b = p0.connectionStatus
                if (p1 != null) {
                    Toast.makeText(context,"Tidak terhubung ke server",Toast.LENGTH_LONG).show()
                } else {
                    Log.d("SENDBIRD", "CONNECT")
                    channel_url = arguments?.getString("channel_url")
                    fetchChatItem()
                }

                SendBirdDesk.authenticate("mqsakarais44223", "",object : SendBirdDesk.AuthenticateHandler{
                    override fun onResult(p0: SendBirdException?) {
                        if (p0 != null) {
                            // Error handling.
                            return
                        }
                    }
                })
            }

        })

        // Set up the network to use HttpURLConnection as the HTTP client.
//        val network = BasicNetwork(HurlStack())
//        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
//        requestQueue = RequestQueue(cache, network).apply {
//            start()
//        }

        val layoutManager = LinearLayoutManager(getActivity())
        (view.recycler_chatting.getItemAnimator() as SimpleItemAnimator).setSupportsChangeAnimations(false)
        view.recycler_chatting.setLayoutManager(layoutManager)
        //mRecyclerView.addItemDecoration(DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL))


        view.send_msg.isEnabled =  FALSE


        view.text_msg.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if (view.text_msg.text == null || view.text_msg.text.toString() == "") {
                    view.send_msg.isEnabled =  FALSE
                } else {
                    view.send_msg.isEnabled =  TRUE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                //
            }
        })
        view.send_msg.setOnClickListener {
//            Log.d("TEST",testadapter.itemCount.toString())
//            val time = Datetime(System.currentTimeMillis())
//            add new messages
            channel!!.sendUserMessage(view.text_msg.text.toString(),object : BaseChannel.SendUserMessageHandler{
                override fun onSent(p0: UserMessage?, p1: SendBirdException?) {
                    if (p1 == null){
                        if (adapter.itemCount == 0) {
                            adapter.add(ChattingItemTo(p0!!.message,Datetime(p0.createdAt)!!))
//                            adapter.add(ChattingItemTo(view.text_msg.text.toString(),time!!))
                            view.recycler_chatting.adapter = adapter
                            view.recycler_chatting.scrollToPosition(adapter.getItemCount()-1)

                        } else {
                            adapter.add(ChattingItemTo(p0!!.message,Datetime(p0.createdAt)!!))
//                            adapter.add(ChattingItemTo(view.text_msg.text.toString(),time!!))
                            adapter.notifyItemInserted(adapter.itemCount-1)
                            view.recycler_chatting.scrollToPosition(adapter.getItemCount()-1)
                        }

                        view.text_msg.setText("")

                    } else {
                        Toast.makeText(context,"Pesan tidak terkirim",Toast.LENGTH_LONG).show()
                    }
                }

            })
        }

        SendBird.addChannelHandler(UNIQUE_HANDLER_ID,object : SendBird.ChannelHandler() {

            override fun onMessageReceived(p0: BaseChannel?, p1: BaseMessage?) {
                if (p1 is UserMessage){

                    if(p1.channelUrl==channel_url){
                        if (adapter.itemCount == 0) {
                            adapter.add(ChattingItemFrom(p1.message,Datetime(p1.createdAt)!!))
//                            adapter.add(ChattingItemTo(view.text_msg.text.toString(),time!!))
                            view.recycler_chatting.adapter = adapter
                            view.recycler_chatting.scrollToPosition(adapter.getItemCount()-1)

                        } else {
                            adapter.add(ChattingItemFrom(p1.message,Datetime(p1.createdAt)!!))
//                            adapter.add(ChattingItemTo(view.text_msg.text.toString(),time!!))
                            adapter.notifyItemInserted(adapter.itemCount-1)
                            view.recycler_chatting.scrollToPosition(adapter.getItemCount()-1)
                        }
                        channel!!.markAsRead()
                    }
                } else if (p1 is FileMessage) {

                } else if (p1 is AdminMessage) {

                }
            }
        })
        // Inflate the layout for this fragment
        return view
    }

    private fun Datetime(lastmsgtime: Long): String? {
        var sdf = SimpleDateFormat("YYYY-MM-dd", Locale("in", "ID"))
        val timestamp = Timestamp(lastmsgtime)

        val nowdate = Date(System.currentTimeMillis())
        val netDate = Date(timestamp.time)

        val nowtime = sdf.format(nowdate)
        val time = sdf.format(netDate)

        if (time==nowtime){
            sdf = SimpleDateFormat("HH:mm", Locale("in", "ID"))
            val timetext = sdf.format(netDate)
            return timetext
        } else {
            sdf = SimpleDateFormat("MMMM dd", Locale("in", "ID"))
            val timetext = sdf.format(netDate)
            return timetext
        }

    }

    private fun fetchChatItem() {
        GroupChannel.getChannel(channel_url, object: GroupChannel.GroupChannelGetHandler{
            override fun onResult(p0: GroupChannel?, p1: SendBirdException?) {
                if (p0 != null){
                    channel = p0
                    val msglist = p0.createPreviousMessageListQuery()
                    msglist.load(100, false,object : PreviousMessageListQuery.MessageListQueryResult{
                        override fun onResult(p0: MutableList<BaseMessage>?, p1: SendBirdException?) {
                            if (p1 != null){
                                return
                            } else {
                                for (j in 0 until p0!!.size){
                                    val msg = p0.get(j)
                                    val usermsg = msg as UserMessage
                                    val sender = usermsg.sender.userId
                                    val msgtext = usermsg.message
                                    val timetext = Datetime(usermsg.createdAt)
                                    Log.d("DATA-"+j,msgtext+", sender : "+sender)
                                    if(sender=="mqsakarais44223"){
                                        adapter.add(ChattingItemTo(msgtext,timetext!!))
                                    } else {
                                        adapter.add(ChattingItemFrom(msgtext,timetext!!))
                                    }
                                }
                                view!!.recycler_chatting.adapter = adapter
                                view!!.recycler_chatting.scrollToPosition(p0.size-1)
                            }
                        }

                    })
                }
            }
        })
        //requestQueue!!.add(jsonArrayRequest)

    }

    override fun onDestroy() {

        SendBird.removeChannelHandler(UNIQUE_HANDLER_ID)

        super.onDestroy()
    }
    override fun onDestroyView() {
        Log.d("TEST DESTROY VIEW","SUCCESS")
        super.onDestroyView()
    }
}

class ChattingItemTo(val msg: CharSequence? = "", val time: String) : Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.msg_to.text = msg
        viewHolder.itemView.time_to.text = time

    }
    override fun getLayout(): Int {
        return R.layout.fragment_chat_to
    }

}

class ChattingItemFrom(val msg: CharSequence? = "", val time: String) : Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.msg_from.text = msg
        viewHolder.itemView.time_from.text = time
    }
    override fun getLayout(): Int {
        return R.layout.fragment_chat_from
    }

}


