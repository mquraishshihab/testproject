package com.example.testproject

import android.content.Context
import android.graphics.Rect
import android.media.ExifInterface
import android.net.Uri
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnticipateInterpolator
import android.view.animation.Interpolator
import android.widget.*
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.*
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayout
import com.omadahealth.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_search.view.*
import kotlinx.android.synthetic.main.fragment_search_list.view.*
import kotlinx.android.synthetic.main.fragment_transaction_list.view.*
import org.json.JSONArray
import org.json.JSONObject


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
//private const val AUTH_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEyYjEyYzFlNmEwYmRkOTE1NzQ0ODhjMzExNjFhNzUzYzk1NWZmNWI3ZmNmMTFiMGU0YjY4MzUxMDQ0N2RjOTUwMTVmNDcyYTRkMGFhNDkzIn0.eyJhdWQiOiIxIiwianRpIjoiYTJiMTJjMWU2YTBiZGQ5MTU3NDQ4OGMzMTE2MWE3NTNjOTU1ZmY1YjdmY2YxMWIwZTRiNjgzNTEwNDQ3ZGM5NTAxNWY0NzJhNGQwYWE0OTMiLCJpYXQiOjE1NjE2MjIxNTQsIm5iZiI6MTU2MTYyMjE1NCwiZXhwIjoxNTkzMjQ0NTU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.WqOnIYqsr5oP0wuV7Vw9Rcaa0N1K1T05mbiFz6Lj9M5_Tu8J_nZ-_M2wcY-OZAF0nX6_QPOoiwxYLh3KEXpIGEb2lPWbunLO_pBD29-OeRxIZGcB3bJuHaQqFYKSohqcY0mhgA4BDTxT2nLJt5ibfAaUef2Tbji_2v298ut7NJ1sCGF1EXzA-5Lpa7K1Pgn5iADTK8un5VEZSdNZceB862n417sD06N226GTVOfA1r3FiP88T0ZQbt_fHEJYnPJIDHiPyZBFVkHFEh6HmgVT6gFITdUakegt7uqIJf5NeKu1oeBITZKFZ7Lkk_rzuNYuArePnBHqfoUwZJPT8ohrCFLuk-Dd7wuxXTevRf3pBQJ8NHwW3fPUTWcPKNKgw89S4RLYy7x_H0P5s3NpKgSTrOfGxANkWIJhh6QEKygMpyiNfBfFW6kKAR05xg32xTUC--FKJObG53jkeihpQ9fbfdeR4JT6aUOOCPCTVS5JePfnV6Gum8MRGr6NxoMiWga4T6Fq4rBODcH-T9x5pUc7rTIOx1sRJwxb0A3iK6Ib9i0AG7HUvz9tgk7IgzLd93r1You2TSrPeXSYPGnmPZUCvzYH7UcTTE5Xz-iwgNLSuSGhe7x9eCIeKDzFnfioVFH5z4MJWevVbB9pkeTBkFEmZDtMsnrbJwy2r8qpIVLxsV0"
private const val AUTH_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImY0ZmUyZGU5MGMzYzJiMDQ2OTM3YmU3NTIxYzliNTliNzFkMTkyNjM2ZGE5MDBhM2E4YTM1N2FiYjg3NGIzZDc2N2VmMWVkOWI4ZjY3YjVkIn0.eyJhdWQiOiIxIiwianRpIjoiZjRmZTJkZTkwYzNjMmIwNDY5MzdiZTc1MjFjOWI1OWI3MWQxOTI2MzZkYTkwMGEzYThhMzU3YWJiODc0YjNkNzY3ZWYxZWQ5YjhmNjdiNWQiLCJpYXQiOjE1NjIzOTg2MjgsIm5iZiI6MTU2MjM5ODYyOCwiZXhwIjoxNTk0MDIxMDI4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.pokvkEft7RtRWIhDJPyLlBKuG3s8FwKxfs2qix7ae9AIpaawQsql8dW1v93-OEGLXxND3yuYi4ehWkb3jxZheibTK1qsROfQNbyQn4hGNM-Qsr76JjqQGh2eTQW470pjCrFecF-mWotDDqwuh6NnWJRKZP4DJqS3tixKwzRS568idAlMbxDxWGm5zeGAZyoxMUmOSIR5rVDXEEupsCCl2rqcHrLpILMyiKDM_wCC5AtN6Xf8GJmiFSEwlWiXmFqFMyjQtGO3CRkhAu0K2DHvCzSo2W00mRTr5JpWC9MqAW5lrf6fpJpNifxbqR3pVbrCQ1dm15KyglTtP0O_OpaCyqtuAACeT8gcM6TkSfhdvxLjvSHuInb1gOVZVKuHLisy71_3T3idKI5MBXu3GhOflhWE-sZJGmMdzXQ_xnktK9E1GdOT1WaJonfVN9ankUWJQT76bAugXDYF2Ef_o3kBVcp-Gk0ei2eZ-0awPjeZsVB6gEBYMKfuNBjHh6kN7MTfdlHl_dUkRZjj2rHADRcZwF0wyexBsKxOK2_W8KViR0L7Fvy1GQps_KZtPbMNIkjxjUmcAzjQxYXX0l8lrOei_qbiXD5htdCJpUfb_u5mP-2vilEtDIcEUJQMLAiiOdec_5G9m8aXBiSml6Maz-lsvNjRsyiLCXDNpJ5CdgZdpRc"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [SearchFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [SearchFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class SearchFragment : Fragment() {
    // TODO: Rename and change types of parameters

    var kategoriSort = arrayOf("Paling Sesuai", "Paling Sering Disewa", "Jarak terdekat dari lokasi", "Harga Terendah", "Harga Tertinggi")
    var url = "https://armysploit.tk/api/v1/search?"
    private var requestQueue : RequestQueue? = null
    val response : JSONArray? = null
    private var id : String? = null
    private var img : String? = null
    private var vendor_id : String? = null
    private var vendor_name : String? = null
    private var product_name : String? = null
    private var price : String? = null
    private var location : String? = null
    private var rating : String? = null
    private var ascending = true
    private var mConstraintLayout : ConstraintLayout? = null
    var page = 1
    private var query : String? = null
    private var startdate : String? = null
    private var qty : String? = null
    private var category : String? = null
    private var type : String? = null
    private var enddate : String? = null
    private var from : String? = null
    private var to : String? = null
    var adapter = GroupAdapter<ViewHolder>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_search, container, false)
         query = arguments?.getString("query")
         startdate = arguments?.getString("start_date")
         qty = arguments?.getString("qty")
         category = arguments?.getString("category")
         type = arguments?.getString("type")
         from = arguments?.getString("from")
         to = arguments?.getString("to")
        Log.d("TOTAL", arguments!!.size().toString())

        if (arguments!!.size() >5 ) {
            enddate = arguments?.getString("end_date")

            url =
                "https://armysploit.tk/api/v1/search?query=" + query + "&start_date=" + startdate + "&end_date=" + enddate + "&qty=" + qty + "&category=" + category + "&type=" + type + "&page="
            Log.d("a:", url.toString())

        } else if (category.toString()== "towing"){
                url = "https://armysploit.tk/api/v1/search?from="+from+"&start_date="+startdate+"&to="+to+"&category="+category+"&qty="
        } else {
            url = "https://armysploit.tk/api/v1/search?query="+query+"&start_date="+startdate+"&qty="+qty+"&category="+category+"&type="+type+"&page="
            Log.d("b:",url.toString())
        }

//        else
//            url = "https://armysploit.tk/api/v1/search?from="+from+"&start_date="+startdate+"&end_date="+enddate+"&to"+to+"&category="+category+"&qty="+qty



        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        val network = BasicNetwork(HurlStack())
        val mRecyclerView = view.findViewById<RecyclerView>(R.id.recviewSearch)
        val sort = view.findViewById<Button>(R.id.textView29)
        val spinnerSort = view. findViewById<Spinner>(R.id.spinnerSearch)

        //Spinner SORT
        // Create an ArrayAdapter using a simple spinnerSort layout and languages array
        val aa = ArrayAdapter(activity, android.R.layout.simple_spinner_item, kategoriSort)
        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        spinnerSort.adapter = aa
//      adapter.clear or removeAll
        spinnerSort.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(arg0: AdapterView<*>, arg1: View?, position: Int, id: Long) {
//                textView_msg!!.text = "Selected : "+kategoriMobil[position]
            }
            override fun onNothingSelected(arg0: AdapterView<*>) {
            }
        }

        val layoutManager = GridLayoutManager(getActivity(),2)
        (mRecyclerView.getItemAnimator() as SimpleItemAnimator).setSupportsChangeAnimations(false)
        mRecyclerView.setLayoutManager(layoutManager)

        requestQueue = RequestQueue(cache, network).apply {
            start()
        }
        val swipesearch = view.findViewById<SwipyRefreshLayout>(R.id.swipe_search)
        swipesearch.setOnRefreshListener(object : SwipyRefreshLayout.OnRefreshListener{

            override fun onRefresh(direction: SwipyRefreshLayoutDirection?) {
                fetchreqres(mRecyclerView, adapter)
            }
        })
        fetchreqres(mRecyclerView, adapter)
        // Inflate the layout for this fragment
        return view
    }


    private fun fetchreqres(
        mRecyclerView: RecyclerView,
        adapter: GroupAdapter<ViewHolder>
    ) {
        //url = address+page
        val address = url+page
        Log.d("Address",address)
        val jsonObjectRequest = object : JsonObjectRequest(
            Method.GET, address, null,
            Response.Listener<JSONObject> { response ->

               // val jsonArray = response.getJSONArray("data")//
//                val error = response.getString("error")
//                    if (error!=null){
//                        Log.d("ERROR",error)
//                    }

                val data = response.getJSONObject("data")
                val products = data.getJSONObject("products")

                val datatrue = products.getJSONArray("data")
                if (datatrue.length() < 1){
                    Toast.makeText(context!!, "HABIS", Toast.LENGTH_LONG).show()
                } else {
                    page = products.getString("current_page").toInt()+1

                    for(i in 0 until datatrue.length()) {
                        val jsonObject: JSONObject = datatrue.getJSONObject(i)

                        id = jsonObject.getString("id")
                        img = jsonObject.getString("image_uri")
                        vendor_id = jsonObject.getString("vendor_id")
                        vendor_name = jsonObject.getString("vendor_name")
                        product_name = jsonObject.getString("product_name")
                        price = jsonObject.getString("price")
                        location = jsonObject.getString("location")
                        rating = jsonObject.getString("rating")

                        Log.d("ID"+i,jsonObject.getString("product_name"))
                        adapter.add(SearchItem(id!!,product_name, img, vendor_name,"Rp. "+price))

                    }

                    adapter.setOnItemClickListener { item, view ->

                        val searchItem = item as SearchItem
                        val bundle = Bundle()
                        bundle.putString("id", searchItem.id)
                        view.findNavController().navigate(R.id.action_searchFragment_to_detailMobilFragment,bundle)
                    }
                    Log.d("adapter ", page.toString())
                    if (page <= 2){
                        mRecyclerView.adapter= adapter
                    } else {
                        adapter.notifyDataSetChanged()
                        mRecyclerView.smoothScrollBy(125,125,AnticipateInterpolator())
                    }
                }

                view!!.swipe_search.isRefreshing = false

            },
            Response.ErrorListener {
                Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
//                headers.put("authorization","Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEyYjEyYzFlNmEwYmRkOTE1NzQ0ODhjMzExNjFhNzUzYzk1NWZmNWI3ZmNmMTFiMGU0YjY4MzUxMDQ0N2RjOTUwMTVmNDcyYTRkMGFhNDkzIn0.eyJhdWQiOiIxIiwianRpIjoiYTJiMTJjMWU2YTBiZGQ5MTU3NDQ4OGMzMTE2MWE3NTNjOTU1ZmY1YjdmY2YxMWIwZTRiNjgzNTEwNDQ3ZGM5NTAxNWY0NzJhNGQwYWE0OTMiLCJpYXQiOjE1NjE2MjIxNTQsIm5iZiI6MTU2MTYyMjE1NCwiZXhwIjoxNTkzMjQ0NTU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.WqOnIYqsr5oP0wuV7Vw9Rcaa0N1K1T05mbiFz6Lj9M5_Tu8J_nZ-_M2wcY-OZAF0nX6_QPOoiwxYLh3KEXpIGEb2lPWbunLO_pBD29-OeRxIZGcB3bJuHaQqFYKSohqcY0mhgA4BDTxT2nLJt5ibfAaUef2Tbji_2v298ut7NJ1sCGF1EXzA-5Lpa7K1Pgn5iADTK8un5VEZSdNZceB862n417sD06N226GTVOfA1r3FiP88T0ZQbt_fHEJYnPJIDHiPyZBFVkHFEh6HmgVT6gFITdUakegt7uqIJf5NeKu1oeBITZKFZ7Lkk_rzuNYuArePnBHqfoUwZJPT8ohrCFLuk-Dd7wuxXTevRf3pBQJ8NHwW3fPUTWcPKNKgw89S4RLYy7x_H0P5s3NpKgSTrOfGxANkWIJhh6QEKygMpyiNfBfFW6kKAR05xg32xTUC--FKJObG53jkeihpQ9fbfdeR4JT6aUOOCPCTVS5JePfnV6Gum8MRGr6NxoMiWga4T6Fq4rBODcH-T9x5pUc7rTIOx1sRJwxb0A3iK6Ib9i0AG7HUvz9tgk7IgzLd93r1You2TSrPeXSYPGnmPZUCvzYH7UcTTE5Xz-iwgNLSuSGhe7x9eCIeKDzFnfioVFH5z4MJWevVbB9pkeTBkFEmZDtMsnrbJwy2r8qpIVLxsV0")
//                headers["accept"] = "application/json"
//                headers["content-Type"] = "application/json"
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImY0ZmUyZGU5MGMzYzJiMDQ2OTM3YmU3NTIxYzliNTliNzFkMTkyNjM2ZGE5MDBhM2E4YTM1N2FiYjg3NGIzZDc2N2VmMWVkOWI4ZjY3YjVkIn0.eyJhdWQiOiIxIiwianRpIjoiZjRmZTJkZTkwYzNjMmIwNDY5MzdiZTc1MjFjOWI1OWI3MWQxOTI2MzZkYTkwMGEzYThhMzU3YWJiODc0YjNkNzY3ZWYxZWQ5YjhmNjdiNWQiLCJpYXQiOjE1NjIzOTg2MjgsIm5iZiI6MTU2MjM5ODYyOCwiZXhwIjoxNTk0MDIxMDI4LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.pokvkEft7RtRWIhDJPyLlBKuG3s8FwKxfs2qix7ae9AIpaawQsql8dW1v93-OEGLXxND3yuYi4ehWkb3jxZheibTK1qsROfQNbyQn4hGNM-Qsr76JjqQGh2eTQW470pjCrFecF-mWotDDqwuh6NnWJRKZP4DJqS3tixKwzRS568idAlMbxDxWGm5zeGAZyoxMUmOSIR5rVDXEEupsCCl2rqcHrLpILMyiKDM_wCC5AtN6Xf8GJmiFSEwlWiXmFqFMyjQtGO3CRkhAu0K2DHvCzSo2W00mRTr5JpWC9MqAW5lrf6fpJpNifxbqR3pVbrCQ1dm15KyglTtP0O_OpaCyqtuAACeT8gcM6TkSfhdvxLjvSHuInb1gOVZVKuHLisy71_3T3idKI5MBXu3GhOflhWE-sZJGmMdzXQ_xnktK9E1GdOT1WaJonfVN9ankUWJQT76bAugXDYF2Ef_o3kBVcp-Gk0ei2eZ-0awPjeZsVB6gEBYMKfuNBjHh6kN7MTfdlHl_dUkRZjj2rHADRcZwF0wyexBsKxOK2_W8KViR0L7Fvy1GQps_KZtPbMNIkjxjUmcAzjQxYXX0l8lrOei_qbiXD5htdCJpUfb_u5mP-2vilEtDIcEUJQMLAiiOdec_5G9m8aXBiSml6Maz-lsvNjRsyiLCXDNpJ5CdgZdpRc"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequest)
    }

    override fun onDestroy() {
        page = 1
        adapter.clear()
        requestQueue!!.cache.clear()
        super.onDestroy()
    }

    override fun onDestroyView() {
        page = 1
        adapter.clear()
        requestQueue!!.cache.clear()
        super.onDestroyView()
    }
}

class SearchItem(val id : String,val barang: CharSequence? = "", val img: CharSequence? = "",val bodi: CharSequence?="", val detail: CharSequence?="") : Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.title_id.text = barang
        viewHolder.itemView.body.text = bodi
        viewHolder.itemView.detail.text = detail
        Picasso.get().load(img.toString()).resize(200,200).into(viewHolder.itemView.img_id)
    }
    override fun getLayout(): Int {
        return R.layout.fragment_search_list
    }

    //override fun getSpanSize(spanCount: Int, position: Int) = spanCount / 3

}


class SpaceDecoration : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val position = parent.getChildAdapterPosition(view)
        outRect.left = when(position) {
            0, 3 -> 5 // Left most two items
            else -> 10 // others
        }
        outRect.right = when(position) {
            2, 5 -> 5 // Right most two items
            else ->0 // others
        }
        outRect.top = 10
        outRect.bottom = 0
    }


}

//    private fun fetchTransaction(
//        mRecyclerView: RecyclerView,
//        adapter: GroupAdapter<ViewHolder>
//    ) {
//
//        val jsonArrayRequest = JsonArrayRequest(
//            Request.Method.GET, url, null,
//            Response.Listener<JSONArray> { response ->
//                //val jumlah = response.length()
//                for(i in 0..response.length()-1) {
//                    val jsonObject: JSONObject = response.getJSONObject(i)
//
//                    barangtext = jsonObject.getString("nama")
//                    statustranstext = jsonObject.getString("hari")
//                    hargatext = jsonObject.getString("tanggal")
//                    statuspaytext = jsonObject.getString("keterangan")
//
//                    adapter.add(SearchItem(barangtext))
//
//                }
//
//                adapter.setOnItemClickListener { item, view ->
//                    view.findNavController().navigate(R.id.action_searchFragment_to_detailMobilFragment)
//              }
//
//                mRecyclerView.adapter= adapter
//
//            },
//            Response.ErrorListener {
//                Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
//            })
//        jsonArrayRequest.retryPolicy = DefaultRetryPolicy(
//            7500,
//            // 0 means no retry
//            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
//            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
//        )
//
//        requestQueue!!.add(jsonArrayRequest)
//
//    }