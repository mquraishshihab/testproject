package com.example.testproject


import android.app.PendingIntent.getActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SimpleItemAnimator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonArrayRequest
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_notification_list.*
import kotlinx.android.synthetic.main.fragment_notification_list.view.*
import kotlinx.android.synthetic.main.fragment_transaction_list.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Boolean.FALSE
import java.security.AccessController.getContext


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class NotificationFragment : Fragment() {

    val url = "https://presensi-dinas.000webhostapp.com/absensi/user/history.php?nip=999999999999999999"
    private var requestQueue : RequestQueue? = null
    val response : JSONArray? = null
    private var barangtext : String? = null
    private var timetext : String? = null
    private var descriptiontext : String? = null
    private var statustext : String? = null
    private var idtext : String? = null
    private var mConstraintLayout : ConstraintLayout? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater!!.inflate(R.layout.fragment_notification, container, false)
        val adapter = GroupAdapter<ViewHolder>()

        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        mConstraintLayout = view.findViewById<ConstraintLayout>(R.id.constraintLayout)
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())
        val mRecyclerView = view.findViewById<RecyclerView>(R.id.recycler_notification)
        mRecyclerView!!.isNestedScrollingEnabled = FALSE
        val layoutManager = LinearLayoutManager(getActivity())
        (mRecyclerView.getItemAnimator() as SimpleItemAnimator).setSupportsChangeAnimations(false)
        mRecyclerView.setLayoutManager(layoutManager)

        requestQueue = RequestQueue(cache, network).apply {
            start()
        }

        fetchNotification(mRecyclerView, adapter)

        // Inflate the layout for this fragment
        return view
    }

    private fun fetchNotification(
        mRecyclerView: RecyclerView,
        adapter: GroupAdapter<ViewHolder>
    ) {

        val jsonArrayRequest = JsonArrayRequest(
            Request.Method.GET, url, null,
            Response.Listener<JSONArray> { response ->
                //val jumlah = response.length()
                for(i in 0..response.length()-1) {
                    val jsonObject: JSONObject = response.getJSONObject(i)

                    barangtext = jsonObject.getString("nama")
                    timetext = jsonObject.getString("hari")
                    descriptiontext = jsonObject.getString("keterangan")
                    statustext = jsonObject.getString("tanggal")
                    idtext = jsonObject.getString("id")

                    adapter.add(NotificationItem(i,barangtext, timetext, statustext, descriptiontext, idtext))

                }

                adapter.setOnItemClickListener { item, view ->

                    val notificationItem = item as NotificationItem
                    val bundle = Bundle()
                    bundle.putString("TEST", notificationItem.item.toString())
                    val n=0
                    val id_notif = notificationItem.id.toString().toInt()
                    if (id_notif % 2 == 0 ) {
                        view.findNavController().navigate(R.id.action_notificationFragment_to_detailTransactionFragment, bundle)
                    } else {
                        view.findNavController().navigate(R.id.action_notificationFragment_to_detailHistoryFragment, bundle)
                    }
                }

                mRecyclerView.adapter= adapter
            },
            Response.ErrorListener {
                Toast.makeText(context, "Error", Toast.LENGTH_LONG).show()
            })
        jsonArrayRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonArrayRequest)



    }

}

class NotificationItem(val num : Int,val item: CharSequence? = "",val time: CharSequence? = "",val stat: CharSequence? = "",val desc: CharSequence? = ""
                       ,val id: CharSequence? = "") : Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {
        val url_img = "https://picsum.photos/id/"+num+"/400/400"
        viewHolder.itemView.item_notif.text = item
        viewHolder.itemView.time_notif.text = time
        viewHolder.itemView.stat_notif.text = stat
        viewHolder.itemView.desc_notif.text = desc
        Picasso.get().load(url_img).resize(200,200).into(viewHolder.itemView.img_notif)

    }
    override fun getLayout(): Int {
        return R.layout.fragment_notification_list
    }


}
