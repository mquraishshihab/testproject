package com.example.testproject


import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class SettingNotificationFragment : Fragment() {
    private var requestQueue : RequestQueue? = null
    var switch_chat : Switch? = null
    var switch_disc : Switch? = null
    var switch_review : Switch? = null
    var switch_admin : Switch? = null
    var switch_news : Switch? = null
    var url_settingnotif = "https://armysploit.tk/api/v1/settings/notification"
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_setting_notification, container, false)
        switch_chat = view.findViewById(R.id.switch_chat_notif)
        switch_disc = view.findViewById(R.id.switch_discuss_notif)
        switch_review = view.findViewById(R.id.switch_review_notif)
        switch_admin = view.findViewById(R.id.switch_admin_notif)
        switch_news = view.findViewById(R.id.switch_news_notif)

        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())
//        switch_chat!!.isChecked = TRUE
//        switch_disc!!.isChecked = TRUE
//        switch_review!!.isChecked = TRUE
//        switch_admin!!.isChecked = TRUE
//        switch_news!!.isChecked = TRUE

        switch_chat!!.setOnCheckedChangeListener { buttonView, isChecked ->

//            The switch is enabled/checked
            requestQueue = RequestQueue(cache, network).apply {
                start()
            }
            UpdateSetting("chat", isChecked)
        }
        switch_admin!!.setOnCheckedChangeListener { buttonView, isChecked ->
            requestQueue = RequestQueue(cache, network).apply {
                start()
            }
            UpdateSetting("chat_admin", isChecked)
        }
        switch_disc!!.setOnCheckedChangeListener { buttonView, isChecked ->
            requestQueue = RequestQueue(cache, network).apply {
                start()
            }
            UpdateSetting("discussion", isChecked)
        }
        switch_news!!.setOnCheckedChangeListener { buttonView, isChecked ->
            requestQueue = RequestQueue(cache, network).apply {
                start()
            }
            UpdateSetting("news", isChecked)
        }
        switch_review!!.setOnCheckedChangeListener { buttonView, isChecked ->
            requestQueue = RequestQueue(cache, network).apply {
                start()
            }
            UpdateSetting("review", isChecked)
        }


        requestQueue = RequestQueue(cache, network).apply {
            start()
        }

        fetchSetting()
        return view
    }

    private fun UpdateSetting(s: String, checked: Boolean) {

        Log.d("KEY", s)
        Log.d("STATE", checked.toString())

        val params = HashMap<String,Any>()
        params["key"] = s
        params["state"] = checked
        val jsonObject = JSONObject(params)
        var state: Boolean? = null
        val request =  object : JsonObjectRequest(
            Method.PUT,url_settingnotif,jsonObject,
            Response.Listener { response ->
                // Process the json
                Log.d("RESPONSE", response.toString())
                if (response.getInt("status_code") == 201){

                   val data = response.getJSONObject("data")

                    if(data.getInt("state")==0){
                        state = false
                    } else {
                        state = true
                    }
                    Toast.makeText(context,data.getString("key")+" is "+state,Toast.LENGTH_LONG).show()
                } else {
                    when(s){
                        "chat" -> {
                            switch_chat!!.isChecked = !checked
                        }
                        "discussion" -> {
                            switch_disc!!.isChecked = !checked
                        }
                        "review" -> {
                            switch_review!!.isChecked = !checked
                        }
                        "chat_admin" -> {
                            switch_admin!!.isChecked = !checked
                        }
                        "news" -> {
                            switch_news!!.isChecked = !checked
                        }
                    }
                    //error
                    Toast.makeText(context,"Terjadi kesalahan pada server", Toast.LENGTH_SHORT).show()
                }
                requestQueue!!.stop()
            }, Response.ErrorListener{
                // Error in request

                when(s){
                    "chat" -> {
                        switch_chat!!.isChecked = !checked
                    }
                    "discussion" -> {
                        switch_disc!!.isChecked = !checked
                    }
                    "review" -> {
                        switch_review!!.isChecked = !checked
                    }
                    "chat_admin" -> {
                        switch_admin!!.isChecked = !checked
                    }
                    "news" -> {
                        switch_news!!.isChecked = !checked
                    }
                }

                requestQueue!!.stop()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()

                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            2, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue!!.add(request)
    }

    private fun fetchSetting() {
        val jsonObjectRequest = object : JsonObjectRequest(
            Request.Method.GET, url_settingnotif, null,
            Response.Listener<JSONObject> { response ->
                val data = response.getJSONObject("data")

                val chat = data.getBoolean("chat")
                val discussion = data.getBoolean("discussion")
                val review = data.getBoolean("review")
                val chat_admin = data.getBoolean("chat_admin")
                val news = data.getBoolean("news")

                switch_chat!!.isChecked = chat
                switch_disc!!.isChecked = discussion
                switch_review!!.isChecked = review
                switch_admin!!.isChecked = chat_admin
                switch_news!!.isChecked = news

            },
            Response.ErrorListener {
                Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequest)
    }


}
