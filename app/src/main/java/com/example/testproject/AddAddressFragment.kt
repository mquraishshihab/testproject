package com.example.testproject


import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Address
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.mindorks.editdrawabletext.DrawablePosition
import com.mindorks.editdrawabletext.EditDrawableText
import com.mindorks.editdrawabletext.onDrawableClickListener
import kotlinx.android.synthetic.main.fragment_add_address.*
import kotlinx.android.synthetic.main.fragment_add_address.view.*
import org.json.JSONObject
import java.lang.Boolean.FALSE
import java.lang.Boolean.TRUE


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val REQUEST_CHECK_SETTINGS = 2
private const val LOCATION_REQ_CODE = 1
private var locationUpdateState = false
/**
 * A simple [Fragment] subclass.
 *
 */
class AddAddressFragment : Fragment(), OnMapReadyCallback {
    private var requestQueue : RequestQueue? = null
    var countproc: Int? = null
    lateinit var mapFragment: SupportMapFragment
    lateinit var mGoogleMap: GoogleMap
    var addressline : List<Address>? = null
    var geocoder: Geocoder? = null
    var addresstext: String? = null
    var marker: Marker? = null
    var locationManager : LocationManager? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    private lateinit var locationRequesthighaccuracy: LocationRequest
    private lateinit var locationRequestbalanced: LocationRequest
    private lateinit var builder: LocationSettingsRequest.Builder
    private lateinit var result:  Task<LocationSettingsResponse>
    private lateinit var loc_response:  LocationSettingsResponse

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater!!.inflate(R.layout.fragment_add_address, container, false)
        countproc = 0
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)

        locationManager = activity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        mapFragment = childFragmentManager!!.findFragmentById(R.id.map_add_address) as SupportMapFragment
        mapFragment.getMapAsync(this)
        geocoder = Geocoder(getActivity())

        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())
        requestQueue = RequestQueue(cache, network).apply {
            start()
        }

        view.save_add_address.setOnClickListener {
            if(view.addresstext_add_address.length() < 6){
                view.addresstext_add_address.error = "Alamat terlalu singkat"
            } else {
                requestQueue = RequestQueue(cache, network).apply {
                    start()
                }
                AddAddress()
            }
        }

        view.addresstext_add_address.setDrawableClickListener(object : onDrawableClickListener {
            override fun onClick(target: DrawablePosition) {
                when (target) {
                    DrawablePosition.RIGHT -> {
                        //mapFragment.getMapAsync(OnMapReadyCallback {
                            addresstext = addresstext_add_address.text.toString()
                            if (addresstext!!.isEmpty() || addresstext == null){
                                addresstext_add_address.setError("Please fill out this field")
                                addresstext_add_address.requestFocus()
                            } else {
                                addressline = geocoder!!.getFromLocationName(addresstext,1)
                                if((addressline as MutableList<Address>?)!!.size > 0) {
                                    val address = addressline!!.get(0)
                                    val lat = address!!.latitude
                                    val lng = address!!.longitude
                                    val place = LatLng(lat,lng)
                                    if (marker==null) {
                                        marker = mGoogleMap.addMarker(MarkerOptions().position(place))
                                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place,18.0f),1500,null)
                                    } else {
                                        mGoogleMap.clear()
                                        marker = mGoogleMap.addMarker(MarkerOptions().position(place))
                                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place,18.0f),1000,null)
                                    }

                                } else {
                                    Toast.makeText(activity, "Alamat tidak dapat ditemukan", Toast.LENGTH_LONG).show()
                                }
                            }

                    }

                }
            }
        })


        return view
    }

    private fun AddAddress() {
        val url_add = "https://armysploit.tk/api/v1/address/add"
        val params = HashMap<String,String>()
        params["address"] = view!!.addresstext_add_address.text.toString()
        params["lat"] = mGoogleMap.cameraPosition.target.latitude.toString()
        params["lng"] = mGoogleMap.cameraPosition.target.longitude.toString()
        val jsonObject = JSONObject(params)

        val request =  object : JsonObjectRequest(
            Method.POST,url_add,jsonObject,
            Response.Listener { response ->
                // Process the json
                Log.d("RESPONSE", response.toString())
                if (response.getInt("status_code") == 200){

                    view!!.findNavController().navigateUp()
                } else {
                    //error
                    Log.d("ERROR CODE", response.getInt("status_code").toString())
                }
                requestQueue!!.stop()
            }, Response.ErrorListener{
                // Error in request
                requestQueue!!.stop()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            2, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue!!.add(request)
    }

    private fun RequestLocation(){
        locationRequesthighaccuracy = LocationRequest.create()
        locationRequesthighaccuracy.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        locationRequestbalanced = LocationRequest.create()
        locationRequestbalanced.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
        builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequesthighaccuracy).addLocationRequest(locationRequestbalanced)
        builder.setNeedBle(TRUE)
        result = LocationServices.getSettingsClient(activity!!).checkLocationSettings(builder.build())
        result.addOnSuccessListener {

            setCurrentLocation()
        }
        result.addOnFailureListener {e ->
            if (e is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    e.startResolutionForResult(activity,
                        REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }

        }
    }
    private fun setCurrentLocation() {
        if (!locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            RequestLocation()
        } else {
            if (ActivityCompat.checkSelfPermission(activity!!,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQ_CODE)
                return
            }
//
            mGoogleMap.isMyLocationEnabled = true
            fusedLocationClient.lastLocation.addOnSuccessListener {
                if (it != null){
                    lastLocation = it
                    val edit_address = view!!.findViewById<EditDrawableText>(R.id.addresstext_add_address)
                    val Latlng = LatLng(it.latitude, it.longitude)
                    if (marker==null) {

                        addressline = geocoder!!.getFromLocation(it.latitude,it.longitude,1)
                        if((addressline as MutableList<Address>?)!!.size > 0) {
                            addresstext = (addressline as MutableList<Address>?)!!.get(0).getAddressLine(0)
                            edit_address.setText(addresstext)
                            marker = mGoogleMap.addMarker(MarkerOptions().position(Latlng))
                            edit_address.setText(addresstext)
                            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Latlng,18.0f))
                            edit_address.setText(addresstext)
                            //mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Latlng,18.0f))
                        } else {
                            edit_address.setText("")
                        }
                    } else {
                        mGoogleMap.clear()
                        marker = mGoogleMap.addMarker(MarkerOptions().position(Latlng))
                        edit_address.setText(addresstext)
                        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Latlng,18.0f), 1500, null)
                    }
                }
            }
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == Activity.RESULT_OK) {
                setCurrentLocation()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            LOCATION_REQ_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED)) {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    view!!.findNavController().navigateUp()
                    Toast.makeText(context, "Perlu izin akses Lokasi",Toast.LENGTH_LONG).show()

                } else {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    setCurrentLocation()

                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mGoogleMap = googleMap

        LocationPermission()

        mGoogleMap.setOnMyLocationButtonClickListener(GoogleMap.OnMyLocationButtonClickListener {
            setCurrentLocation()
            return@OnMyLocationButtonClickListener false
        })

            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(-2.051010, 115.878276),3.5f))
            mGoogleMap.setOnCameraMoveListener(object : GoogleMap.OnCameraMoveListener {
                override fun onCameraMove() {
                    if (marker!=null){
                        Log.d("MAP", "UPDATE")
                        marker!!.position = mGoogleMap.cameraPosition.target

                    } else {

                        Log.d("MAP", "BUAT")
                        marker = mGoogleMap.addMarker(MarkerOptions().position(mGoogleMap.cameraPosition.target))
                        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mGoogleMap.cameraPosition.target,18.0f))
                    }
                    //mGoogleMap.clear()


                }
            })

        mGoogleMap.setOnCameraIdleListener(object : GoogleMap.OnCameraIdleListener {
            override fun onCameraIdle() {
                if (marker!=null){
                    val latitude = mGoogleMap.cameraPosition.target.latitude
                    val longitude = mGoogleMap.cameraPosition.target.longitude
                    Log.d("KOORDINAT", ""+latitude+"    "+longitude)
                    val edit_address = view!!.findViewById<EditDrawableText>(R.id.addresstext_add_address)
                    addressline = geocoder!!.getFromLocation(latitude,longitude,1)
                    if((addressline as MutableList<Address>?)!!.size > 0) {
                        addresstext = (addressline as MutableList<Address>?)!!.get(0).getAddressLine(0)

                        if (countproc!! > 0) {
                            edit_address.setText(addresstext)
                        }
                        countproc = countproc!!+1
                    } else {
                        edit_address.setText("")
                    }

                }


            }
        })
            /* Drag Marker
            mGoogleMap.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
                override fun onMarkerDragEnd(p0: Marker?) {
                    val latitude = p0!!.position.latitude
                    val longitude = p0!!.position.longitude
                    Log.d("Latitude : ", latitude.toString())
                    Log.d("Longitude : ", longitude.toString())
                }

                override fun onMarkerDragStart(p0: Marker?) {
                    val latitude = p0!!.position.latitude
                    val longitude = p0!!.position.longitude
                    Log.d("Latitude : ", latitude.toString())
                    Log.d("Longitude : ", longitude.toString())
                }

                override fun onMarkerDrag(p0: Marker?) {
                    val latitude = p0!!.position.latitude
                    val longitude = p0!!.position.longitude
                    Log.d("Latitude : ", latitude.toString())
                    Log.d("Longitude : ", longitude.toString())
                }
            })
            */
    }

    private fun LocationPermission() {
        if (ActivityCompat.checkSelfPermission(activity!!,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQ_CODE)
            return
        }
        mGoogleMap.isMyLocationEnabled = true
    }


}
