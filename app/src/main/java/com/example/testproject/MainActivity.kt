package com.example.testproject

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.util.Log
import android.view.Menu
import android.app.Application
import android.net.wifi.hotspot2.pps.Credential
import android.preference.Preference
import android.view.MenuItem
import android.view.View
import android.support.v7.widget.Toolbar
//import android.widget.Toolbar
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.*
import com.sendbird.android.SendBird
import com.sendbird.android.SendBirdException
import com.sendbird.android.User

import com.sendbird.desk.android.SendBirdDesk
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Boolean.FALSE
import java.lang.Boolean.TRUE
import java.util.prefs.Preferences

class MainActivity : AppCompatActivity() {
    private val APP_ID = "8CA8F389-73D3-47B7-BCB3-923165256A25"
    private var navController : NavController? = null
    val numb = "3"
    var userConnect : User.ConnectionStatus? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.back_button_24dp)

        //SENDBIRD CODE
        SendBird.init(APP_ID,applicationContext)
        SendBirdDesk.init()

        SendBird.connect("mqsakarais44223", object : SendBird.ConnectHandler{
            override fun onConnected(p0: User?, p1: SendBirdException?) {

                if (p1 != null) {

                    return
                } else {
                    Log.d("USER ID", p0!!.nickname)

                    val b = p0.connectionStatus
                    userConnect = b
                }
                SendBirdDesk.authenticate("mqsakarais44223","", object : SendBirdDesk.AuthenticateHandler{
                    override fun onResult(p0: SendBirdException?) {
                        if (p0 != null) {
                            // Error handling.
                            return
                        }
                    }
                })
            }

        })
        if(userConnect != User.ConnectionStatus.OFFLINE || userConnect != User.ConnectionStatus.NON_AVAILABLE) {
            SendBird.setChannelInvitationPreference(TRUE, object : SendBird.SetChannelInvitationPreferenceHandler{
                override fun onResult(p0: SendBirdException?) {
                    if (p0 != null) {    // Error.
                        return
                    }
                }
            })
        }
        //SENDBIRD CODE


        val bottomNavigationView : BottomNavigationView = findViewById(R.id.bottomNavigationView)

        navController = findNavController(R.id.nav_host_fragment)
        bottomNavigationView.setupWithNavController(navController!!)
        setupActionBarWithNavController(navController!!)
        NavigationUI.setupWithNavController(bottomNavigationView, navController!!)
        navController!!.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.detailTransactionFragment -> { toolbar.visibility = View.VISIBLE
                    bottomNavigationView.visibility = View.GONE }

                R.id.transactionFragment -> { toolbar.visibility = View.VISIBLE
                    bottomNavigationView.visibility = View.VISIBLE
                    bottomNavigationView.menu.getItem(2).setCheckable(true)
//                    supportActionBar!!.setBackgroundDrawable(ColorDrawable(getResources().getColor(R.color.biruDorenmi)))
//                    supportActionBar!!.setTitle("hehe")
                    supportActionBar!!.setIcon(android.R.color.transparent)
                }
                R.id.homeFragment -> { toolbar.visibility = View.VISIBLE
                    bottomNavigationView.menu.getItem(0).setCheckable(true)
//                    supportActionBar?.setIcon(R.mipmap.ic_logodorenmi_putih)
                    bottomNavigationView.visibility = View.VISIBLE
                }
                R.id.chatFragment -> { toolbar.visibility = View.VISIBLE

                    for (i in 0..bottomNavigationView.menu.size()-1)
                    {
                        bottomNavigationView.menu.getItem(i).setCheckable(false)
                    }

                    bottomNavigationView.visibility = View.VISIBLE }
                R.id.notificationFragment -> { toolbar.visibility = View.VISIBLE
                    for (i in 0..bottomNavigationView.menu.size()-1)
                    {
                        bottomNavigationView.menu.getItem(i).setCheckable(false)
                    }
                    bottomNavigationView.visibility = View.VISIBLE }
                R.id.cartFragment -> { toolbar.visibility = View.VISIBLE
                    bottomNavigationView.menu.getItem(1).setCheckable(true)
                    bottomNavigationView.visibility = View.VISIBLE }
                R.id.historyFragment -> { toolbar.visibility = View.VISIBLE
                    bottomNavigationView.menu.getItem(3).setCheckable(true)
                    bottomNavigationView.visibility = View.VISIBLE }
                R.id.accountFragment -> { toolbar.visibility = View.VISIBLE
                    bottomNavigationView.menu.getItem(4).setCheckable(true)
                    bottomNavigationView.visibility = View.VISIBLE }
                R.id.searchFragment -> { toolbar.visibility = View.VISIBLE
                    bottomNavigationView.menu.getItem(4).setCheckable(true)
                    bottomNavigationView.visibility = View.VISIBLE }
                R.id.detailMobilFragment -> { toolbar.visibility = View.VISIBLE
                    bottomNavigationView.menu.getItem(4).setCheckable(true)
                    bottomNavigationView.visibility = View.GONE }
            }

            /*
            if(destination.id == R.id.detailTransactionFragment) {
                //toolbar.visibility = View.VISIBLE
                //bottomNavigationView.visibility = View.GONE
            } else {
               // toolbar.visibility = View.GONE
               // bottomNavigationView.visibility = View.VISIBLE
            }
            */

        }

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.action_bar, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(navController!!) || super.onOptionsItemSelected(item)
    }

    override fun onSupportNavigateUp(): Boolean {
        return findNavController(R.id.nav_host_fragment).navigateUp()
    }

}
