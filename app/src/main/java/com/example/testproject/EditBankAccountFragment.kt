package com.example.testproject


import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.sendbird.android.shadow.com.google.gson.JsonArray
import kotlinx.android.synthetic.main.fragment_edit_bank_account.view.*
import org.json.JSONObject


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class EditBankAccountFragment : Fragment() {
    private var requestQueue : RequestQueue? = null
    var url_bank = "https://armysploit.tk/api/v1/balance/bank-list"
    var url_update = "https://armysploit.tk/api/v1/balance/update"

    var bank_namelist = ArrayList<String>()
    var bank_idlist = ArrayList<String>()
    var id : String? = null
    var account_name : String? = null
    var account_number : String? = null
    var bank_name : String? = null
    var bank : String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View = inflater.inflate(R.layout.fragment_edit_bank_account, container, false)
        if (arguments?.size()!! > 0) {
            id = arguments?.getString("id")
            account_name = arguments?.getString("account_name")
            account_number = arguments?.getString("account_number")
            bank_name = arguments?.getString("bank_name")
            url_update = "https://armysploit.tk/api/v1/balance/"+id+"/update"
        } else {
            view.findNavController().navigateUp()
        }


        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())
        requestQueue = RequestQueue(cache, network).apply {
            start()
        }
        fetchBank()

//        view.balnum_edit_bank.setText(account_number)
//        view.owner_edit_bank.setText(account_name)
        view.bankname_edit_bank.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                bank = bank_idlist.get(position)
            }
        }

        view.save_edit_bank.setOnClickListener {
            if (bank == null || bank == "" || view.balnum_edit_bank.length() < 10 || view.owner_edit_bank.length() <= 1){
                Toast.makeText(context, "Please input correctly", Toast.LENGTH_SHORT).show()

            } else {
                requestQueue = RequestQueue(cache, network).apply {
                    start()
                }
                EditBankAccount(bank!!)
            }
        }
        // Inflate the layout for this fragment

        return view
    }

    private fun EditBankAccount(bank: String) {

        val params = HashMap<String,String>()
        params["account_name"] = view!!.owner_edit_bank.text.toString()
        params["account_number"] = view!!.balnum_edit_bank.text.toString()
        params["bank_id"] = bank
        val jsonObject = JSONObject(params)

        val request =  object : JsonObjectRequest(
            Method.PUT,url_update,jsonObject,
            Response.Listener { response ->
                // Process the json
                Log.d("RESPONSE", response.toString())
                if (response.getInt("status_code") == 201){

                    view!!.findNavController().navigateUp()
                } else {
                    //error
                    Log.d("ERROR CODE", response.getInt("status_code").toString())
                }
                requestQueue!!.stop()
            }, Response.ErrorListener{
                // Error in request
                requestQueue!!.stop()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()

                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            2, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue!!.add(request)
    }

    private fun fetchBank() {
        val jsonObjectRequest = object : JsonObjectRequest(
            Method.GET, url_bank, null,
            Response.Listener<JSONObject> { response ->
                val data = response.getJSONArray("data")

                if (data.length()>0){
                    bank_idlist.add("")
                    bank_namelist.add("Pilih Bank")
                    for (i in 0 until data.length()){
                        val jsonObject = data.getJSONObject(i)

                        val bank_id = jsonObject.getString("bank_id")
                        val bank_name = jsonObject.getString("bank_name")
                        bank_idlist.add(bank_id)
                        bank_namelist.add(bank_name)
                    }

                    var arrayAdapter = object : ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, bank_namelist){

                        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
                            val tv = super.getDropDownView(position, convertView, parent) as TextView

                            // Set the text color of drop down items
                            tv.setTextColor(Color.BLACK)

                            // If this item is selected item
                            if (position == 0) {
                                // Set spinner selected popup item's text color
                                tv.setTextColor(Color.GRAY)
                            }
                            return tv
                        }
                    }
//                    view!!.bankname_add_bank.adapter = arrayAdapter
                    view!!.bankname_edit_bank.adapter = arrayAdapter
                    view!!.balnum_edit_bank.setText(account_number)
                    view!!.owner_edit_bank.setText(account_name)
                    for (i in 0 until bank_namelist.size){
                        if (bank_name == bank_namelist.get(i)){
                            view!!.bankname_edit_bank.setSelection(i)
                            bank = bank_idlist.get(i)
                        }
                    }
                } else {
                    Toast.makeText(context,"Tidak ada bank yang tersedia", Toast.LENGTH_LONG).show()
                }
                //experiment
//                if (total_amount>0){
//                    val filters = arrayOf<InputFilter>(InputFilter.LengthFilter(total_amount ?: 0))
//                    view!!.price_withdraw.filters = filters
//                } else {
//                    val filters = arrayOf<InputFilter>(InputFilter.LengthFilter(1 ?: 0))
//                    view!!.price_withdraw.filters = filters
//                }

//                total_amount = total_amount
                requestQueue!!.stop()
            },
            Response.ErrorListener {
                Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
                requestQueue!!.stop()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequest)
    }


}
