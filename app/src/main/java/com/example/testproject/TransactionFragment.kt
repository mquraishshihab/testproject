package com.example.testproject


import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SimpleItemAnimator
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnticipateInterpolator
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder

//android volley
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_history.view.*
import kotlinx.android.synthetic.main.fragment_history_list.view.*
import kotlinx.android.synthetic.main.fragment_transaction.view.*
import kotlinx.android.synthetic.main.fragment_transaction_list.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Boolean
import java.lang.Boolean.FALSE


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class TransactionFragment : Fragment() {


    // Instantiate the RequestQueue with the cache and network. Start the queue.
    var adapter = GroupAdapter<ViewHolder>()
    var address : String? = null
    val url = "https://armysploit.tk/api/v1/orders?page="
    var page = 1
    private var requestQueue : RequestQueue? = null
    val response : JSONArray? = null
    private var barangtext : String? = null
    private var statustranstext : String? = null
    private var hargatext : String? = null
    private var statuspaytext : String? = null
    private var mConstraintLayout : ConstraintLayout? = null

    //private var requestQueue : RequestQueue? = null
    //private var requestQueue : RequestQueue? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater!!.inflate(R.layout.fragment_transaction, container, false)

        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        mConstraintLayout = view.findViewById<ConstraintLayout>(R.id.constraintLayout)
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())
        val mRecyclerView = view.findViewById<RecyclerView>(R.id.recycler_transaction)

        val layoutManager = LinearLayoutManager(getActivity())
        (mRecyclerView.getItemAnimator() as SimpleItemAnimator).setSupportsChangeAnimations(false)
        mRecyclerView.setLayoutManager(layoutManager)
        mRecyclerView.addItemDecoration(DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL))

        view.swipe_trans.setOnRefreshListener {
            requestQueue = RequestQueue(cache, network).apply {
                start()
            }
            fetchTransaction(mRecyclerView)
        }
        requestQueue = RequestQueue(cache, network).apply {
            start()
        }

        fetchTransaction(mRecyclerView)
        //recycler_transaction.layoutManager = LinearLayoutManager(activity)

        // Inflate the layout for this fragment
        return view
    }

    override fun onDestroyView() {
        requestQueue!!.cache.clear()
        page = 1
        super.onDestroyView()
    }
    private fun fetchTransaction(
        mRecyclerView: RecyclerView
    ) {
        address = url+page
        val jsonObjectRequest = object : JsonObjectRequest(
            Method.GET, address, null,
            Response.Listener<JSONObject> { response ->

                val data = response.getJSONObject("data")

                val datatrue = data.getJSONArray("data")
                if (datatrue.length() < 1){
                    Toast.makeText(context!!, "Tidak ada data yang tersedia", Toast.LENGTH_LONG).show()
                    view!!.swipe_trans.isEnabled = FALSE
                } else {
                    page = data.getString("current_page").toInt()+1
                    for(i in adapter!!.itemCount until datatrue.length()) {
                        val jsonObject: JSONObject = datatrue.getJSONObject(i)

                        val img = jsonObject.getString("image_uri")
                        val invoice = jsonObject.getString("invoice_no")
                        val transaction_status = jsonObject.getString("transaction_status")
                        val payment_status = jsonObject.getString("payment_status")
                        val product_name = jsonObject.getString("product_name")
                        val price = jsonObject.getString("charge")

                        adapter!!.add(TransactionItem(invoice, product_name, transaction_status, payment_status,price,img ))
                    }

                    adapter!!.setOnItemClickListener { item, view ->
                        val transactionItem = item as TransactionItem
                        val bundle = Bundle()
                        bundle.putString("invoice", transactionItem.invoice.toString())
                        view.findNavController().navigate(R.id.action_transactionFragment_to_detailTransactionFragment, bundle)
                    }
                    if (data.getInt("current_page")==data.getInt("last_page")) {
                        view!!.swipe_trans.isEnabled = FALSE
                    }
                    if (page!! <= 2){
                        mRecyclerView.adapter= adapter
                    } else {
                        adapter!!.notifyDataSetChanged()
                        mRecyclerView.smoothScrollBy(125,125, AnticipateInterpolator())
                    }
                }
                view!!.swipe_trans.isRefreshing = FALSE

                requestQueue!!.stop()
                //val jumlah = response.length()

            },
            Response.ErrorListener {
                Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
                view!!.swipe_trans.isRefreshing = FALSE

                requestQueue!!.stop()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()

//                headers.put("authorization","Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEyYjEyYzFlNmEwYmRkOTE1NzQ0ODhjMzExNjFhNzUzYzk1NWZmNWI3ZmNmMTFiMGU0YjY4MzUxMDQ0N2RjOTUwMTVmNDcyYTRkMGFhNDkzIn0.eyJhdWQiOiIxIiwianRpIjoiYTJiMTJjMWU2YTBiZGQ5MTU3NDQ4OGMzMTE2MWE3NTNjOTU1ZmY1YjdmY2YxMWIwZTRiNjgzNTEwNDQ3ZGM5NTAxNWY0NzJhNGQwYWE0OTMiLCJpYXQiOjE1NjE2MjIxNTQsIm5iZiI6MTU2MTYyMjE1NCwiZXhwIjoxNTkzMjQ0NTU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.WqOnIYqsr5oP0wuV7Vw9Rcaa0N1K1T05mbiFz6Lj9M5_Tu8J_nZ-_M2wcY-OZAF0nX6_QPOoiwxYLh3KEXpIGEb2lPWbunLO_pBD29-OeRxIZGcB3bJuHaQqFYKSohqcY0mhgA4BDTxT2nLJt5ibfAaUef2Tbji_2v298ut7NJ1sCGF1EXzA-5Lpa7K1Pgn5iADTK8un5VEZSdNZceB862n417sD06N226GTVOfA1r3FiP88T0ZQbt_fHEJYnPJIDHiPyZBFVkHFEh6HmgVT6gFITdUakegt7uqIJf5NeKu1oeBITZKFZ7Lkk_rzuNYuArePnBHqfoUwZJPT8ohrCFLuk-Dd7wuxXTevRf3pBQJ8NHwW3fPUTWcPKNKgw89S4RLYy7x_H0P5s3NpKgSTrOfGxANkWIJhh6QEKygMpyiNfBfFW6kKAR05xg32xTUC--FKJObG53jkeihpQ9fbfdeR4JT6aUOOCPCTVS5JePfnV6Gum8MRGr6NxoMiWga4T6Fq4rBODcH-T9x5pUc7rTIOx1sRJwxb0A3iK6Ib9i0AG7HUvz9tgk7IgzLd93r1You2TSrPeXSYPGnmPZUCvzYH7UcTTE5Xz-iwgNLSuSGhe7x9eCIeKDzFnfioVFH5z4MJWevVbB9pkeTBkFEmZDtMsnrbJwy2r8qpIVLxsV0")
//                headers["accept"] = "application/json"
//                headers["content-Type"] = "application/json"
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequest)
    }

}

class TransactionItem(val invoice: CharSequence? = "",val product_name: CharSequence? = "",val transaction_status:
    CharSequence? = "",val payment_status: CharSequence? = "",val price : CharSequence? = "",val img : CharSequence? = "")
    : Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.item_trans.text = product_name
        viewHolder.itemView.deal_trans.text = transaction_status
        viewHolder.itemView.pay_trans.text = payment_status
        viewHolder.itemView.price_trans.text = "Rp."+price
        Picasso.get().load(img.toString()).resize(200,200).into(viewHolder.itemView.img_trans)
    }
    override fun getLayout(): Int {
        return R.layout.fragment_transaction_list
    }

}


