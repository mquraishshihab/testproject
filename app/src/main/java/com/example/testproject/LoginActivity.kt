package com.example.testproject

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.example.testproject.Data.LoginSession
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.facebook.FacebookCallback
import com.facebook.CallbackManager
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.*
import com.google.android.gms.common.SignInButton
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import java.lang.Boolean.TRUE
import java.util.*


class LoginActivity : AppCompatActivity() {
    var loginSession:LoginSession? = null
    private var requestQueue : RequestQueue? = null
    private val RC_SIGN_IN = 9001
    var login: Button? = null
//    var loginFacebook: LoginButton? = null
    var loginGoogle: SignInButton? = null
    var daftar: Button? = null
    var user: EditText? = null
    var pass: EditText? = null
    var signInIntent: Intent? = null
    val EMAIL = "email"
    private var callbackManager : CallbackManager? = null
    private var mGoogleSignInClient : GoogleSignInClient? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        loginSession = LoginSession(this)
        if (loginSession!!.getLogin_status()!!) {
            startActivity(
                Intent(this@LoginActivity, MainActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            )
            finish()
        }
        callbackManager = CallbackManager.Factory.create()

        //VOLLEY REQUEST
        val cache = DiskBasedCache(this.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())
        requestQueue = RequestQueue(cache, network).apply {
            start()

        }


        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        login = findViewById(R.id.buttonlogin)
        login?.setOnClickListener({
            doLogin()

        })

        daftar = findViewById(R.id.buttondaftar)
        daftar?.setOnClickListener({
            var intent2 = Intent (this,SignUpActivity::class.java)
            startActivity (intent2)
        })

        loginGoogle = findViewById(R.id.sign_in_button) as SignInButton
        loginGoogle!!.setSize(SignInButton.SIZE_STANDARD)
        loginGoogle!!.setOnClickListener({
            signIn()
        })

//        loginFacebook = findViewById(R.id.login_button) as LoginButton
//        loginFacebook!!.setReadPermissions(Arrays.asList(EMAIL))
//        // If you are using in a fragment, call loginButton.setFragment(this);
//        // Callback registration
//        loginFacebook!!.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
//            override fun onSuccess(loginResult: LoginResult) {
//                // App code
//            }
//
//            override fun onCancel() {
//                // App code
//            }
//
//            override fun onError(exception: FacebookException) {
//                // App code
//            }
//        })
    }
    fun doLogin(){
        val url ="https://armysploit.tk/api/v1/login"

        val params = HashMap<String,String>()
//        params["email"] = "lugassawan@gmail.com"
//        params["password"] = "coba12345"
        params["email"] = editText.text.toString()
        params["password"] = editText2.text.toString()
        val jsonObject = JSONObject(params)

        val request = JsonObjectRequest(
            Request.Method.POST,url,jsonObject,
            Response.Listener { response ->
                // Process the json
                var data = response.getJSONObject("data")
                val email = data.getString("email")
                val token = data.getString("token")
                loginSession!!.setEmail(loginSession!!.email_session,email)
                loginSession!!.setToken(loginSession!!.token_session,token)
                loginSession!!.setLogin_status(loginSession!!.login_status_session,TRUE)
                var intent = Intent (this,MainActivity::class.java)
                startActivity (intent)
                Log.d("a:", token.toString())


            }, Response.ErrorListener{
                // Error in request
                Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
            })


        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            2, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue!!.add(request)

    }
    private fun signIn() {
        val signInIntent = mGoogleSignInClient!!.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode === RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val result : GoogleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result)
        }else{
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
            super.onActivityResult(requestCode, resultCode, data)
        }

    }

    private fun handleSignInResult(signInResult: GoogleSignInResult) {
        if(signInResult.isSuccess) {
            // Authenticated
            val account : GoogleSignInAccount? = signInResult.signInAccount
        } else {
            // Failed
        }
    }
}
