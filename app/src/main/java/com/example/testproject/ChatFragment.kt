package com.example.testproject


import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.sendbird.android.*
import com.sendbird.desk.android.SendBirdDesk
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_chat.view.*
import kotlinx.android.synthetic.main.fragment_chat_list_recent.view.*
import kotlinx.android.synthetic.main.fragment_chat_list_unread.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Boolean.FALSE
import java.sql.Time
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val UNIQUE_HANDLER_ID = "dorenmi_chat"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ChatFragment : Fragment() {

    var address : String? = null
    val url = "https://armysploit.tk/api/v1/chat/"
    private var requestQueue : RequestQueue? = null
    val response : JSONArray? = null
    private var msgtext : String? = null
    private var vendortext : String? = null
    private var timetext : String? = null
    private var counttext : String? = null
    private var mConstraintLayout : ConstraintLayout? = null

    var constrain: ConstraintLayout? = null
    var recyclerunread : RecyclerView? =null
    var recyclerrecent : RecyclerView? =null

    var adapterrecent = GroupAdapter<ViewHolder>()
    var adapterunread = GroupAdapter<ViewHolder>()

    var userConnect : User.ConnectionStatus? = null
    var listchannel : MutableList<GroupChannel>? = null
    var groupChannelListQuery : GroupChannelListQuery? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?)

            : View? {
         //Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_chat, container, false)

        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        mConstraintLayout = view.findViewById<ConstraintLayout>(R.id.constraintLayout)
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())

        recyclerunread = view.findViewById<RecyclerView>(R.id.recycler_unread)
        recyclerrecent = view.findViewById<RecyclerView>(R.id.recycler_recent)

        recyclerunread!!.isNestedScrollingEnabled = FALSE
        recyclerrecent!!.isNestedScrollingEnabled = FALSE

        SendBird.connect("mqsakarais44223", object : SendBird.ConnectHandler{
            override fun onConnected(p0: User?, p1: SendBirdException?) {

                //val channelquery = p0.connectionStatus
                if (p1 != null) {
                    return
                } else {
                    Log.d("SENDBIRD", "CONNECT")
                    requestQueue = RequestQueue(cache, network).apply {
                        start()
                    }
                    fetchChatItem()
                }

                SendBirdDesk.authenticate("mqsakarais44223", "",object : SendBirdDesk.AuthenticateHandler{
                    override fun onResult(p0: SendBirdException?) {
                        if (p0 != null) {
                            // Error handling.
                            return
                        }
                    }
                })
            }

        })

        SendBird.addChannelHandler(UNIQUE_HANDLER_ID,object : SendBird.ChannelHandler() {
            override fun onMessageReceived(p0: BaseChannel?, p1: BaseMessage?) {
                if (p1 is UserMessage){
                    Log.d("INCOMING MSG","SUKSES")
//                    Toast.makeText(context,"INCOMING MSG SUKSES",Toast.LENGTH_LONG).show()

                    val channelurl  = p1.channelUrl
                    Log.d("url-msg", channelurl)
                    Log.d("url-chn", p0!!.url)
                    for (i in 0 until adapterunread.itemCount){
                        if(adapterunread.itemCount >0 ){
                            val itemunread = adapterunread.getItem(i)
                            val unread = itemunread as ChatUnread
                            if (channelurl==unread.channel){
                                Log.d("STATUS", "TAMBAH COUNT")
                                ++unread.count
                                unread.msg=p1.message
                                unread.time = Datetime(p1.createdAt)
                                if (adapterunread.itemCount>1){
                                    adapterunread.remove(unread)
                                    adapterunread.add(0,unread)
                                    adapterunread.notifyDataSetChanged()

                                } else {
                                    adapterunread.notifyItemChanged(i)
                                }
                            }
                        }

//
//                        val itemrecent = adapterrecent.getItem(i)

                    }
                    for (i in 0 until adapterrecent.itemCount){
                        if(adapterrecent.itemCount >0 ){
                            val itemrecent = adapterrecent.getItem(i)
                            val recent = itemrecent as ChatRecent
                            if (channelurl==recent.channel){
                                //recent to unread
                                Log.d("STATUS", "recent to unread")

                                val timetext = Datetime(p1.createdAt)
                                view.unread_chat.visibility = View.VISIBLE
                                recyclerunread!!.visibility = View.VISIBLE
                                adapterunread.add(0,ChatUnread(recent.channel,recent.avatar_url,p1.message,timetext,recent.vendor,1))
                                adapterrecent.remove(itemrecent)
                                adapterrecent.notifyDataSetChanged()
                                adapterunread.notifyItemInserted(adapterunread.itemCount)
//                                if(adapterunread.itemCount>1){
//                                    adapterunread.notifyItemInserted(adapterunread.itemCount)
//                                } else {
//                                    adapterunread.notifyItemInserted(adapterunread.itemCount)
//                                }

                            }
                        }
                    }
                } else if (p1 is FileMessage) {

                } else if (p1 is AdminMessage) {

                }
            }
        })

        val layoutManagerrecent = LinearLayoutManager(getActivity())
        //(recyclerrecent!!.getItemAnimator() as SimpleItemAnimator).setSupportsChangeAnimations(false)
        recyclerrecent!!.setLayoutManager(layoutManagerrecent)


        val layoutManagerunread = LinearLayoutManager(getActivity())
       // (recyclerunread!!.getItemAnimator() as SimpleItemAnimator).setSupportsChangeAnimations(false)
        recyclerunread!!.setLayoutManager(layoutManagerunread)

        //mRecyclerView.addItemDecoration(DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL))


        // Inflate the layout for this fragment
        return view
    }

    private fun fetchGroupChannel(avatar_url: String, display_name: String, channel: String) {
        groupChannelListQuery = GroupChannel.createMyGroupChannelListQuery()
        groupChannelListQuery!!.isIncludeEmpty = FALSE
        groupChannelListQuery!!.next(object : GroupChannelListQuery.GroupChannelListQueryResultHandler{
            override fun onResult(p0: MutableList<GroupChannel>?, p1: SendBirdException?) {
                if (p1 != null) {    // Error.
                    return
                } else {
                    listchannel = p0

                    for(i in 0 until listchannel!!.size) {

                        val channelquery = listchannel!!.get(i)
                        val channel_url = channelquery.url
                        Log.d("Channel", channel)
                        if (channel == channel_url){
                            val msg = channelquery.lastMessage

                            val lastmsg = msg as UserMessage
                            val lastmsgtime = lastmsg.createdAt
                            val timetext =Datetime(lastmsgtime)

                            Log.d("TIME", timetext)
                            val unread = channelquery.unreadMessageCount

                            if (unread > 0) {
                                Log.d("UNREAD", channel_url+" - count :"+unread.toString())
                                adapterunread.add(ChatUnread(channel,avatar_url,lastmsg.message,timetext,display_name,unread))
                            } else {
                                adapterrecent.add(ChatRecent(channel,avatar_url,lastmsg.message,timetext,display_name))
                            }
                        }
//                        for (j in 0 until channelquery.members.size){
//
//                            val member = channelquery.members.get(j).nickname
//                            Log.d("USER", member)
//                        }

//                        val e = channelquery.createPreviousMessageListQuery()
//                        e.load(30, false,object : PreviousMessageListQuery.MessageListQueryResult{
//                            override fun onResult(p0: MutableList<BaseMessage>?, p1: SendBirdException?) {
//                                if (p1 != null){
//                                    return
//                                } else {
//                                    for (j in 0 until p0!!.size){
//                                        val a = p0.get(j)
//                                        val channelquery = a as UserMessage
//                                        val channel_url = channelquery.message
//
//                                        Log.d("DATA-"+j,channel_url)
//                                    }
//                                }
//                            }
//
//                        })
//                        Log.d("Channel URL", channel_url)
//                        Log.d("Unread", channelquery.unreadMessageCount.toString())
//                        channelquery.markAsRead()
//                        d = channelquery.unreadMessageCount

                        //Log.d("Last", lastmsg.message)
                    }

                    recyclerunread!!.adapter = adapterunread

                    if(adapterunread.itemCount > 0 ){
                        recyclerunread!!.visibility = View.VISIBLE

                        view!!.findViewById<TextView>(R.id.unread_chat).visibility = View.VISIBLE
                    } else {
                        recyclerunread!!.visibility = View.GONE
                        view!!.findViewById<TextView>(R.id.unread_chat).visibility = View.GONE
                    }
                    recyclerrecent!!.adapter = adapterrecent

                    if(adapterrecent.itemCount > 0 ){
                        recyclerrecent!!.visibility = View.VISIBLE
                        view!!.findViewById<TextView>(R.id.recent_chat).visibility = View.VISIBLE
                    } else {
                        recyclerrecent!!.visibility = View.GONE
                        view!!.findViewById<TextView>(R.id.recent_chat).visibility = View.GONE
                    }


                }
            }
        })
    }

    private fun Datetime(lastmsgtime: Long): String? {
        var sdf = SimpleDateFormat("YYYY-MM-dd", Locale("in", "ID"))
        val timestamp = Timestamp(lastmsgtime)

        val nowdate = Date(System.currentTimeMillis())
        val netDate = Date(timestamp.time)

        val nowtime = sdf.format(nowdate)
        val time = sdf.format(netDate)

        if (time==nowtime){
            sdf = SimpleDateFormat("HH:mm", Locale("in", "ID"))
            val timetext = sdf.format(netDate)
            return timetext
        } else {
            sdf = SimpleDateFormat("MMMM dd", Locale("in", "ID"))
            val timetext = sdf.format(netDate)
            return timetext
        }

    }

    override fun onDestroyView() {
        SendBird.removeChannelHandler(UNIQUE_HANDLER_ID)

        adapterunread.clear()

        adapterrecent.clear()

        requestQueue!!.cache.clear()

        super.onDestroyView()
    }
    private fun fetchChatItem() {

        val jsonObjectRequest = object : JsonObjectRequest(
            Request.Method.GET, url, null,
            Response.Listener<JSONObject> { response ->
                Log.d("API", "CONNECT")
                val data = response.getJSONArray("data")
                Log.d("LENGTH",data.length().toString())
                for(i in 0 until data.length()) {
                    val jsonObject: JSONObject = data.getJSONObject(i)

                    val avatar_url = jsonObject.getString("avatar_url")
                    val display_name = jsonObject.getString("display_name")
                    val channel = jsonObject.getString("channel")

                    fetchGroupChannel(avatar_url,display_name,channel)

//                    if (i % 3 == 0) {
//                        adapterunr.add(ChatUnread(msgtext,timetext,vendortext,counttext))
//                    } else {
//                        adapterrec.add(ChatRecent(msgtext,timetext,vendortext))
//                    }

                }

                adapterrecent.setOnItemClickListener { item, view ->

                    val chatRecent = item as ChatRecent
                    val bundle = Bundle()
                    bundle.putString("channel_url", chatRecent.channel)

                    view.findNavController().navigate(R.id.action_chatFragment_to_chattingFragment, bundle)
                }

                adapterunread.setOnItemClickListener { item, view ->

                    val chatUnread = item as ChatUnread
                    val bundle = Bundle()
                    bundle.putString("channel_url", chatUnread.channel)
                    GroupChannel.getChannel(chatUnread.channel, object: GroupChannel.GroupChannelGetHandler{
                        override fun onResult(p0: GroupChannel?, p1: SendBirdException?) {
                            if (p0 != null){

                                p0.markAsRead()
                                Log.d(p0.url,"UDAH DI READ")
                            }
                        }
                    })
                    view.findNavController().navigate(R.id.action_chatFragment_to_chattingFragment, bundle)
                }

                requestQueue!!.stop()
            },
            Response.ErrorListener {
                Toast.makeText(context, "Error", Toast.LENGTH_LONG).show()
                requestQueue!!.stop()
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()

//                headers.put("authorization","Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEyYjEyYzFlNmEwYmRkOTE1NzQ0ODhjMzExNjFhNzUzYzk1NWZmNWI3ZmNmMTFiMGU0YjY4MzUxMDQ0N2RjOTUwMTVmNDcyYTRkMGFhNDkzIn0.eyJhdWQiOiIxIiwianRpIjoiYTJiMTJjMWU2YTBiZGQ5MTU3NDQ4OGMzMTE2MWE3NTNjOTU1ZmY1YjdmY2YxMWIwZTRiNjgzNTEwNDQ3ZGM5NTAxNWY0NzJhNGQwYWE0OTMiLCJpYXQiOjE1NjE2MjIxNTQsIm5iZiI6MTU2MTYyMjE1NCwiZXhwIjoxNTkzMjQ0NTU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.WqOnIYqsr5oP0wuV7Vw9Rcaa0N1K1T05mbiFz6Lj9M5_Tu8J_nZ-_M2wcY-OZAF0nX6_QPOoiwxYLh3KEXpIGEb2lPWbunLO_pBD29-OeRxIZGcB3bJuHaQqFYKSohqcY0mhgA4BDTxT2nLJt5ibfAaUef2Tbji_2v298ut7NJ1sCGF1EXzA-5Lpa7K1Pgn5iADTK8un5VEZSdNZceB862n417sD06N226GTVOfA1r3FiP88T0ZQbt_fHEJYnPJIDHiPyZBFVkHFEh6HmgVT6gFITdUakegt7uqIJf5NeKu1oeBITZKFZ7Lkk_rzuNYuArePnBHqfoUwZJPT8ohrCFLuk-Dd7wuxXTevRf3pBQJ8NHwW3fPUTWcPKNKgw89S4RLYy7x_H0P5s3NpKgSTrOfGxANkWIJhh6QEKygMpyiNfBfFW6kKAR05xg32xTUC--FKJObG53jkeihpQ9fbfdeR4JT6aUOOCPCTVS5JePfnV6Gum8MRGr6NxoMiWga4T6Fq4rBODcH-T9x5pUc7rTIOx1sRJwxb0A3iK6Ib9i0AG7HUvz9tgk7IgzLd93r1You2TSrPeXSYPGnmPZUCvzYH7UcTTE5Xz-iwgNLSuSGhe7x9eCIeKDzFnfioVFH5z4MJWevVbB9pkeTBkFEmZDtMsnrbJwy2r8qpIVLxsV0")
//                headers["accept"] = "application/json"
//                headers["content-Type"] = "application/json"
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequest)

    }

}

class ChatUnread(val channel: String, val avatar_url: String, var msg: CharSequence? = "", var time: CharSequence? = "", val vendor: CharSequence? = "",
                 var count: Int) : Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.msg_unread.text = msg
        viewHolder.itemView.time_unread.text = time
        viewHolder.itemView.number_unread.text = count.toString()
        viewHolder.itemView.vendor_unread.text = vendor
        Picasso.get().load(avatar_url).resize(200,200).into(viewHolder.itemView.img_unread)
    }
    override fun getLayout(): Int {
        return R.layout.fragment_chat_list_unread
    }

}

class ChatRecent(val channel: String, val avatar_url: String,
                 var msg: CharSequence? = "", var time: CharSequence? = "", val vendor: CharSequence? = "") : Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.msg_recent.text = msg
        viewHolder.itemView.time_recent.text = time
        viewHolder.itemView.vendor_recent.text = vendor
        Picasso.get().load(avatar_url).resize(200,200).into(viewHolder.itemView.img_recent)
    }
    override fun getLayout(): Int {
        return R.layout.fragment_chat_list_recent
    }

}
