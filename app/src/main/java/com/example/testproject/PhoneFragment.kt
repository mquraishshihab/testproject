package com.example.testproject


import android.os.Bundle
import android.support.v4.app.Fragment
import android.telephony.PhoneNumberUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.volley.*
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import kotlinx.android.synthetic.main.fragment_phone.view.*
import org.json.JSONObject


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class PhoneFragment : Fragment() {
    var status_code : Int? = null
    val url_phone = "https://armysploit.tk/api/v1/profile/phone"
    private var requestQueue : RequestQueue? = null
    var phone : String? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View = inflater.inflate(R.layout.fragment_phone, container, false)
        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())

        if (arguments!!.size() > 0) {
            phone = arguments!!.getString("phone")
            view.old_phone.setText(phone!!)
        } else {
            view.findNavController().navigateUp()
        }
        view.save_phone.setOnClickListener {
            if (view.old_phone.text.toString() == phone){
                if (view.new_phone.length() in 9.rangeTo(13)){
                    val number = view.new_phone.text.toString().subSequence(0,2)
                    if (number == "08"){
                        requestQueue = RequestQueue(cache, network).apply {
                            start()
                        }
//                    Toast.makeText(context,"BENAR",Toast.LENGTH_LONG).show()
                        UpdatePhone()
                    } else {
                        //nanti
                    }
                } else {
                    view.new_phone.error = "Masukkan Nomor HP dengan benar"
                }
            } else {
                view.old_phone.error = "Nomor HP lama anda salah"
            }

        }
        // Inflate the layout for this fragment
        return view
    }

    private fun UpdatePhone() {

        val params = HashMap<String,String>()
        params["phone"] = view!!.new_phone.text.toString()
        val jsonObject = JSONObject(params)

        val request =  object : JsonObjectRequest(
            Method.PUT,url_phone,jsonObject,
            Response.Listener { response ->
                // Process the json
                Log.d("RESPONSE", response.toString())
                if (response.getInt("status_code") == 201){

                    view!!.findNavController().navigateUp()
                } else {
                    //error
                    Log.d("ERROR CODE", response.getInt("status_code").toString())
                }
                requestQueue!!.stop()
            }, Response.ErrorListener{
                // Error in request
                if(status_code==204){
                    view!!.findNavController().navigateUp()
                } else {
                    Toast.makeText(context,"Nomor HP sudah terdaftar",Toast.LENGTH_LONG).show()
                }
                requestQueue!!.stop()
            })
        {
            override fun parseNetworkError(volleyError: VolleyError?): VolleyError {
                status_code = volleyError!!.networkResponse.statusCode
                return super.parseNetworkError(volleyError)
            }
            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject> {
                if (response != null) {
                    status_code = response.statusCode
                }
                return super.parseNetworkResponse(response)
            }

            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["content-type"] = "application/json"
                headers["accept"] = "application/json"
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            2, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue!!.add(request)
    }


}
