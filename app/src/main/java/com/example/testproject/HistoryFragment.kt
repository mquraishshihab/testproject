package com.example.testproject


import android.animation.TimeInterpolator
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SimpleItemAnimator
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnticipateInterpolator
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.*
import com.powyin.scroll.widget.ISwipe
import com.powyin.scroll.widget.SwipeController
import com.powyin.scroll.widget.SwipeRefresh
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import com.zhangxq.refreshlayout.QRefreshLayout
import kotlinx.android.synthetic.main.fragment_history.view.*
import kotlinx.android.synthetic.main.fragment_history_list.view.*
import kotlinx.android.synthetic.main.fragment_transaction_list.view.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Boolean
import java.lang.Boolean.FALSE
import java.lang.Boolean.TRUE


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class HistoryFragment : Fragment() {
    var scrollpos  : Int?=null
    var page : Int? = 1
    var total : Int? = null
    var adapter: GroupAdapter<ViewHolder>? =null
    var address : String? = null
    private var requestQueue : RequestQueue? = null
    val response : JSONArray? = null
    private var barangtext : String? = null
    private var url = "https://armysploit.tk/api/v1/histories?page="
    private var statustranstext : String? = null
    private var hargatext : String? = null
    private var statuspaytext : String? = null
    private var mConstraintLayout : ConstraintLayout? = null
    var swipeController : SwipeController? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater!!.inflate(R.layout.fragment_history, container, false)

        adapter = GroupAdapter<ViewHolder>()

        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
//        mConstraintLayout = view.findViewById<ConstraintLayout>(R.id.constraintLayout)
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())
        val mRecyclerView = view.findViewById<RecyclerView>(R.id.recycler_history)

        val layoutManager = LinearLayoutManager(getActivity())
        (mRecyclerView.getItemAnimator() as SimpleItemAnimator).setSupportsChangeAnimations(false)
        mRecyclerView.setLayoutManager(layoutManager)
        mRecyclerView.addItemDecoration(DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL))
//        view.swipe_history.enableLoadMoreOverScroll(FALSE)
        view.swipe_history.setColorSchemeResources(R.color.dorenmibiru)
        view.swipe_history.setDistanceToTriggerSync(50)
        view.swipe_history.setOnRefreshListener {
            requestQueue = RequestQueue(cache, network).apply {
                start()
            }
            fetchHistory(mRecyclerView)

        }

        requestQueue = RequestQueue(cache, network).apply {
            start()
        }

        fetchHistory(mRecyclerView)

        // Inflate the layout for this fragment
        return view
    }

    private fun fetchHistory(
        mRecyclerView: RecyclerView
    ) {
        address = url+page
        val jsonObjectRequest = object : JsonObjectRequest(
            Method.GET, address, null,
            Response.Listener<JSONObject> { response ->
                Log.d("TEST","SUKSES")
                // val jsonArray = response.getJSONArray("data")//
//                val error = response.getString("error")
//                if (error!=null){
//                    Log.d("ERROR",error)
//                }
//                Log.d("PAGE",url)
                val data = response.getJSONObject("data")

                val datatrue = data.getJSONArray("data")
                if (datatrue.length() < 1){
                    Log.d("PAGE",page.toString())
                    Toast.makeText(context!!, "HABIS", Toast.LENGTH_LONG).show()
                    view!!.swipe_history.isEnabled = FALSE
                } else {
                    page = data.getString("current_page").toInt()+1
                    Log.d("PAGE",page.toString())
                    for(i in adapter!!.itemCount until datatrue.length()) {
                        val jsonObject: JSONObject = datatrue.getJSONObject(i)

//                        val id = jsonObject.getString("id")
                        val img = jsonObject.getString("image_uri")
                        val invoice = jsonObject.getString("invoice_no")
                        val transaction_status = jsonObject.getString("transaction_status")
//                        val vendor_name = jsonObject.getString("vendor_name")
                        val product_name = jsonObject.getString("product_name")
                        val price = jsonObject.getString("charge")

                        adapter!!.add(HistoryItem(invoice, product_name, transaction_status, price,img ))

                    }

                    adapter!!.setOnItemClickListener { item, view ->
                        val historyItem = item as HistoryItem
                        val bundle = Bundle()
                        bundle.putString("invoice", historyItem.invoice.toString())
                        view.findNavController().navigate(R.id.action_historyFragment_to_detailHistoryFragment, bundle)
                    }

                    if (data.getInt("current_page")==data.getInt("last_page")) {
                        view!!.swipe_history.isEnabled = FALSE
                    }

                    if (page!! <= 2){
                        mRecyclerView.adapter= adapter
                    } else {
                        adapter!!.notifyDataSetChanged()
                        mRecyclerView.smoothScrollBy(125,125,AnticipateInterpolator())
                    }
                }
                view!!.swipe_history.isRefreshing = FALSE

                requestQueue!!.stop()
                //val jumlah = response.length()

            },
            Response.ErrorListener {
                Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
                view!!.swipe_history.isRefreshing = FALSE

                requestQueue!!.stop()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()

//                headers.put("authorization","Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImEyYjEyYzFlNmEwYmRkOTE1NzQ0ODhjMzExNjFhNzUzYzk1NWZmNWI3ZmNmMTFiMGU0YjY4MzUxMDQ0N2RjOTUwMTVmNDcyYTRkMGFhNDkzIn0.eyJhdWQiOiIxIiwianRpIjoiYTJiMTJjMWU2YTBiZGQ5MTU3NDQ4OGMzMTE2MWE3NTNjOTU1ZmY1YjdmY2YxMWIwZTRiNjgzNTEwNDQ3ZGM5NTAxNWY0NzJhNGQwYWE0OTMiLCJpYXQiOjE1NjE2MjIxNTQsIm5iZiI6MTU2MTYyMjE1NCwiZXhwIjoxNTkzMjQ0NTU0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.WqOnIYqsr5oP0wuV7Vw9Rcaa0N1K1T05mbiFz6Lj9M5_Tu8J_nZ-_M2wcY-OZAF0nX6_QPOoiwxYLh3KEXpIGEb2lPWbunLO_pBD29-OeRxIZGcB3bJuHaQqFYKSohqcY0mhgA4BDTxT2nLJt5ibfAaUef2Tbji_2v298ut7NJ1sCGF1EXzA-5Lpa7K1Pgn5iADTK8un5VEZSdNZceB862n417sD06N226GTVOfA1r3FiP88T0ZQbt_fHEJYnPJIDHiPyZBFVkHFEh6HmgVT6gFITdUakegt7uqIJf5NeKu1oeBITZKFZ7Lkk_rzuNYuArePnBHqfoUwZJPT8ohrCFLuk-Dd7wuxXTevRf3pBQJ8NHwW3fPUTWcPKNKgw89S4RLYy7x_H0P5s3NpKgSTrOfGxANkWIJhh6QEKygMpyiNfBfFW6kKAR05xg32xTUC--FKJObG53jkeihpQ9fbfdeR4JT6aUOOCPCTVS5JePfnV6Gum8MRGr6NxoMiWga4T6Fq4rBODcH-T9x5pUc7rTIOx1sRJwxb0A3iK6Ib9i0AG7HUvz9tgk7IgzLd93r1You2TSrPeXSYPGnmPZUCvzYH7UcTTE5Xz-iwgNLSuSGhe7x9eCIeKDzFnfioVFH5z4MJWevVbB9pkeTBkFEmZDtMsnrbJwy2r8qpIVLxsV0")
//                headers["accept"] = "application/json"
//                headers["content-Type"] = "application/json"
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequest)
    }

    override fun onDestroy() {

        super.onDestroy()
    }
    override fun onDestroyView() {
        requestQueue!!.cache.clear()
        page = 1
        super.onDestroyView()
    }
}

class HistoryItem(val invoice: CharSequence? = "",val barang: CharSequence? = "",val trans: CharSequence? = "",val harga: CharSequence? = "",val img: CharSequence? = "") : Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.item_hist.text = barang
        viewHolder.itemView.stat_hist.text = trans
        viewHolder.itemView.price_hist.text = "Rp."+harga
        Picasso.get().load(img.toString()).resize(200,200).into(viewHolder.itemView.img_hist)
    }
    override fun getLayout(): Int {
        return R.layout.fragment_history_list
    }

}
