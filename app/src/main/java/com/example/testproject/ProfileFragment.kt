package com.example.testproject


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_profile.view.*
import android.graphics.drawable.ColorDrawable
import android.app.DatePickerDialog
import android.graphics.Color
import android.util.Log
import android.widget.AdapterView
import android.widget.ArrayAdapter
import java.util.*
import android.widget.DatePicker
import android.widget.TextView
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import kotlinx.android.synthetic.main.fragment_profile.*
import org.json.JSONObject
import kotlin.collections.ArrayList


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ProfileFragment : Fragment() {

    private var requestQueue : RequestQueue? = null
    var gender_list = ArrayList<String>()
    var gender_id = ArrayList<String>()

    var datelistener : DatePickerDialog.OnDateSetListener? = null
    var birthdate : String? = null
    var gender : String? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view : View = inflater.inflate(R.layout.fragment_profile, container, false)

//        if (arguments!!.size() > 0) {
//            id = arguments!!.getString("id")
//            initaddress = arguments!!.getString("address")
//            lat = arguments!!.getString("lat")
//
//        } else {
//            view.findNavController().navigateUp()
//        }

        // Inflate the layout for this fragment
        gender_list.add("Jenis Kelamin")
        gender_list.add("Perempuan")
        gender_list.add("Laki-laki")

        gender_id.add("")
        gender_id.add("0")
        gender_id.add("1")

        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())

        var arrayAdapter = object : ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_item, gender_list){

            override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
                val tv = super.getDropDownView(position, convertView, parent) as TextView

                // Set the text color of drop down items
                tv.setTextColor(Color.BLACK)

                // If this item is selected item
                if (position == 0) {
                    // Set spinner selected popup item's text color
                    tv.setTextColor(Color.GRAY)
                }
                return tv
            }
        }
//                    view!!.bankname_add_bank.adapter = arrayAdapter
        view.gender_profile.adapter = arrayAdapter

        view.gender_profile.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                gender = gender_id.get(position)
            }
        }

        view.birth_profile.setOnClickListener {
            val calendar = Calendar.getInstance()
            val year = calendar.get(Calendar.YEAR)
            val month = calendar.get(Calendar.MONTH)
            val day = calendar.get(Calendar.DAY_OF_MONTH)

            datelistener = DatePickerDialog.OnDateSetListener { view, year, month, day ->
                var month = month
                month = month + 1
                if (month >= 10) {
                    if (day >= 10) {
                        birthdate = "$year-$month-$day"
                    } else {
                        birthdate = "$year-$month-0$day"
                    }
                } else {
                    if (day >= 10) {
                        birthdate = "$year-0$month-$day"
                    } else {
                        birthdate = "$year-0$month-0$day"
                    }
                }

                birth_profile.setText(birthdate)
            }

            val  datePickerDialog = DatePickerDialog(
                activity!!,
                android.R.style.Theme_Material_Light_Dialog_MinWidth, datelistener, year, month, day
            )
            //datePickerDialog.getWindow()?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            datePickerDialog.show()


        }

        view.save_profile.setOnClickListener {
            if (view.first_name_profile.length()<3) {
                view.first_name_profile.error = "Nama Depan Terlalu singkat"
            } else if (gender == "" || gender == null) {
                val errorText = view.gender_profile.getSelectedView() as TextView
                errorText.error = ""
                errorText.setTextColor(Color.RED)//just to highlight that this is an error
                errorText.text = "Belum memilih jenis kelamin"
            } else if ( view.birth_profile.text.isBlank()){
                view.birth_profile.error = "Belum memasukkan tanggal lahir"
            } else {
                requestQueue = RequestQueue(cache, network).apply {
                    start()
                }
                updateProfile()
            }

        }

        return view
    }

    private fun updateProfile() {
        val url_profile = "https://armysploit.tk/api/v1/profile/update"
        val params = HashMap<String,String>()
        params["first_name"] = view!!.first_name_profile.text.toString()
        params["last_name"] = view!!.last_name_profile.text.toString()
        params["birthday"] = view!!.birth_profile.text.toString()
        params["gender"] = gender!!
        val jsonObject = JSONObject(params)

        val request =  object : JsonObjectRequest(
            Method.PUT,url_profile,jsonObject,
            Response.Listener { response ->
                // Process the json
                Log.d("RESPONSE", response.toString())
                if (response.getInt("status_code") == 201){

                    view!!.findNavController().navigateUp()
                } else {
                    //error
                    Log.d("ERROR CODE", response.getInt("status_code").toString())
                }
                requestQueue!!.stop()
            }, Response.ErrorListener{
                // Error in request
                requestQueue!!.stop()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()

                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            2, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue!!.add(request)
    }


}
