package com.example.testproject


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SimpleItemAnimator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.squareup.picasso.Picasso
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_detail_history.view.*
import kotlinx.android.synthetic.main.fragment_detail_history_list.view.*
import kotlinx.android.synthetic.main.fragment_history_list.view.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class DetailHistoryFragment : Fragment() {
    var adapter = GroupAdapter<ViewHolder>()
    var url_dethist = "https://armysploit.tk/api/v1/history/"
    private var requestQueue : RequestQueue? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater!!.inflate(R.layout.fragment_detail_history, container, false)

        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        val network = BasicNetwork(HurlStack())
        requestQueue = RequestQueue(cache, network).apply {
            start()
        }
        val layoutManager = LinearLayoutManager(getActivity())
        (view.recycler_detail_history.getItemAnimator() as SimpleItemAnimator).setSupportsChangeAnimations(false)
        view.recycler_detail_history.setLayoutManager(layoutManager)
        view.recycler_detail_history.addItemDecoration(DividerItemDecoration(view.recycler_detail_history.getContext(), DividerItemDecoration.VERTICAL))

        if (arguments!!.size() > 0){
            val invoice = arguments?.getString("invoice")
            url_dethist = url_dethist+invoice
            fetchDetailHistory()
        } else {
            view.findNavController().navigateUp()
        }

        return view
    }

    private fun fetchDetailHistory() {
        val jsonObjectRequest = object : JsonObjectRequest(
            Method.GET, url_dethist, null,
            Response.Listener<JSONObject> { response ->

                if (response.getInt("status_code") == 200) {
                    val data = response.getJSONObject("data")

                    val product = data.getJSONObject("product")

                    val invoice_no = product.getString("invoice_no")
                    val image_uri = product.getString("image_uri")
                    val product_name = product.getString("product_name")
                    val charge = product.getInt("charge")
                    val qty = product.getInt("qty")

                    view!!.invoice_detail_history.text = "Invoice : "+invoice_no
                    Picasso.get().load(image_uri).resize(200,200).into(view!!.image_detail_history)
                    view!!.item_detail_history.text = product_name
                    view!!.qty_detail_history.text = "Jumlah : "+qty.toString()
                    view!!.price_detail_history.text = "Rp."+charge.toString()

                    val description = data.getString("description")
                    view!!.desc_detail_history.text = description

                    val message_list = data.getJSONArray("message_list")
                    for(i in 0 until message_list.length()) {
                        val jsonObject: JSONObject = message_list.getJSONObject(i)

                        val message = jsonObject.getString("message")
                        var created_at = jsonObject.getString("created_at")

                        var sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                        val created = sdf.parse(created_at)
                        sdf = SimpleDateFormat("dd MMMM yyyy, HH:mm", Locale("in", "ID"))
                        val datecreate = sdf.format(created)
                        adapter!!.add(DetailHistoryItem(message,datecreate+" WIB"))
                    }
                    view!!.recycler_detail_history.adapter = adapter
                } else {

                }

            },
            Response.ErrorListener {
                Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequest)
    }

}

class DetailHistoryItem(val message: CharSequence? = "",val created_at: CharSequence? = "") : Item<ViewHolder>(){
    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.msg_detail_history.text = message
        viewHolder.itemView.date_detail_history.text = created_at
    }
    override fun getLayout(): Int {
        return R.layout.fragment_detail_history_list
    }

}
