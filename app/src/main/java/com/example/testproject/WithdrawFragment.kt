package com.example.testproject


import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.InputFilter
import android.text.Spanned
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import kotlinx.android.synthetic.main.fragment_withdraw.view.*
import org.json.JSONObject
import java.lang.Boolean.TRUE


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class WithdrawFragment : Fragment() {
    private var requestQueue : RequestQueue? = null
    var url_withdraw = "https://armysploit.tk/api/v1/balance/withdraw"
    var account_bank = ArrayList<String>()
    var account_id = ArrayList<String>()
    var account : String? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater!!.inflate(R.layout.fragment_withdraw, container, false)

        view.balacc_withdraw.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                account = account_id.get(position)

            }

        }
        view.conf_withdraw.setOnClickListener {
            Log.d("Pos",view.balacc_withdraw.selectedItemPosition.toString())
        }
        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())
        requestQueue = RequestQueue(cache, network).apply {
            start()
        }
        fetchWithdraw()

        view.conf_withdraw.setOnClickListener {
            if (account == null || account == "" || view.price_withdraw.length() < 1 || view.pass_withdraw.length() <= 6){
                Toast.makeText(context, "Please input correctly", Toast.LENGTH_SHORT).show()
            } else {
                requestQueue = RequestQueue(cache, network).apply {
                    start()
                }
                doWithdraw(account!!)
            }

        }

        return view
    }

    private fun doWithdraw(account: String) {
        val url_withdraw = "https://armysploit.tk/api/v1/balance/withdraw"
        val params = HashMap<String,String>()
        params["amount"] = view!!.price_withdraw.text.toString()
        params["password"] = view!!.pass_withdraw.text.toString()
        params["account_bank_id"] = account
        params["check"] = "true"
        val jsonObject = JSONObject(params)

        val request =  object : JsonObjectRequest(
            Method.POST,url_withdraw,jsonObject,
            Response.Listener { response ->
                // Process the json
                Log.d("RESPONSE", response.toString())
                if (response.getInt("status_code") == 200){

                    view!!.findNavController().navigateUp()
                } else {
                    //error
                    Log.d("ERROR CODE", response.getInt("status_code").toString())
                }
                requestQueue!!.stop()
            }, Response.ErrorListener{
                // Error in request
                requestQueue!!.stop()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        // Volley request policy, only one time request to avoid duplicate transaction
        request.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            2, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            1f // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue!!.add(request)
    }

    private fun fetchWithdraw() {

        val jsonObjectRequest = object : JsonObjectRequest(
            Method.GET, url_withdraw, null,
            Response.Listener<JSONObject> { response ->
                val data = response.getJSONObject("data")

                val minimum_withdraw = data.getInt("minimum_withdraw")
                var total_amount = data.getInt("total_amount")
                val acc_bank = data.getJSONArray("account_banks")
                if (acc_bank.length()>0){
                    account_bank.add("Pilih Rekening")
                    account_id.add("")
                    for (i in 0 until acc_bank.length()){
                        val jsonObject = acc_bank.getJSONObject(i)

                        account_bank.add(jsonObject.getString("account_name")+" - "
                                +jsonObject.getString("account_number")+" ("+jsonObject.getString("bank_name")+")")
                        account_id.add(jsonObject.getString("account_id"))
                    }

                    var arrayAdapter = object : ArrayAdapter<String>(context!!, android.R.layout.simple_spinner_item, account_bank){

                        override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
                            val tv = super.getDropDownView(position, convertView, parent) as TextView

                            // Set the text color of drop down items
                            tv.setTextColor(Color.BLACK)

                            // If this item is selected item
                            if (position == 0) {
                                // Set spinner selected popup item's text color
                                tv.setTextColor(Color.GRAY)
                            }
                            return tv
                        }
                    }
                    view!!.balacc_withdraw.adapter = arrayAdapter
                } else {
                    Toast.makeText(context,"Tidak ada rekening yang terdaftar",Toast.LENGTH_LONG).show()
                }
                //experiment
//                if (total_amount>0){
//                    val filters = arrayOf<InputFilter>(InputFilter.LengthFilter(total_amount ?: 0))
//                    view!!.price_withdraw.filters = filters
//                } else {
//                    val filters = arrayOf<InputFilter>(InputFilter.LengthFilter(1 ?: 0))
//                    view!!.price_withdraw.filters = filters
//                }

//                total_amount = total_amount

                view!!.price_withdraw.addTextChangedListener(object : TextWatcher{
                    override fun afterTextChanged(s: Editable?) {
                        if (view!!.price_withdraw.text.length>0){
                            if (view!!.price_withdraw.text.toString().toInt() > total_amount) {

                                view!!.price_withdraw.setText(total_amount.toString())
                                view!!.price_withdraw.setError("Total Saldo anda yaitu Rp."+total_amount,null)
                            }
                        } else {
                            view!!.price_withdraw.setText(minimum_withdraw.toString())
                        }

                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                    }
                })
                requestQueue!!.stop()
            },
            Response.ErrorListener {
                requestQueue!!.stop()
                Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequest.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequest)
    }


}
