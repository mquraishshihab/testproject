package com.example.testproject


import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.findNavController
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.BasicNetwork
import com.android.volley.toolbox.DiskBasedCache
import com.android.volley.toolbox.HurlStack
import com.android.volley.toolbox.JsonObjectRequest
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_account.view.*
import org.json.JSONObject
import com.example.testproject.Data.LoginSession
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_account.*
import android.provider.MediaStore
import java.lang.Boolean


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class AccountFragment : Fragment() {

    var loginSession: LoginSession? = null
    private var mGoogleSignInClient: GoogleSignInClient? = null
    private val CONTENT_ORIENTATION = arrayOf(MediaStore.Images.ImageColumns.ORIENTATION)
    private val IMAGE_REQUEST = 1
    var imageUri: Uri? = null
    val IV: Button? = null
    private val TAG = "PermissionDemo"
    private val RECORD_REQUEST_CODE = 101

    var emailview : TextView? = null
    var phoneview : TextView? = null

    var nameview : TextView? = null
    var birthview : TextView? = null
    var genderview : TextView? = null

    private var url_profile = "https://armysploit.tk/api/v1/profile/edit"
    private var url_account = "https://armysploit.tk/api/v1/profile/user"
    private var requestQueue : RequestQueue? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater!!.inflate(R.layout.fragment_account, container, false)


        val uploadImg = view.findViewById<Button>(R.id.button4)
        val logout = view.findViewById<Button>(R.id.button_logout)

        logout.setOnClickListener{
            signOut()
            loginSession!!.setLogin_status(loginSession!!.login_status_session, false)
            loginSession!!.setEmail(loginSession!!.email_session, "")
            startActivity(
                Intent(activity, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            )
        }

        uploadImg.setOnClickListener({
            val intent = Intent()
                .setType("image/*")
                .setAction(Intent.ACTION_GET_CONTENT)
            startActivityForResult(intent,IMAGE_REQUEST)

        })

        emailview = view.findViewById(R.id.emailaddress_account)
        phoneview = view.findViewById(R.id.phonenumber_account)

        nameview = view.findViewById(R.id.username_account)
        birthview = view.findViewById(R.id.birth_account)
        genderview = view.findViewById(R.id.gender_account)

        view.edit_account.setOnClickListener{
            view.findNavController().navigate(R.id.action_accountFragment_to_profileFragment)
        }

        view.address_account.setOnClickListener{
            view.findNavController().navigate(R.id.action_accountFragment_to_addressFragment)
        }


        //
        view.balance_account.setOnClickListener{
            view.findNavController().navigate(R.id.action_accountFragment_to_balanceFragment)
        }
        view.password_account.setOnClickListener{
            view.findNavController().navigate(R.id.action_accountFragment_to_passwordFragment)
        }
        view.notification_account.setOnClickListener{
            view.findNavController().navigate(R.id.action_accountFragment_to_settingNotificationFragment)
        }
        val cache = DiskBasedCache(activity!!.cacheDir, 1024 * 1024) // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())
        requestQueue = RequestQueue(cache, network).apply {
            start()
        }
        fetchAccount()

        return view
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.data != null) {
            val dialog = AlertDialog.Builder(context)
            dialog.setTitle("Are You Sure!")
            dialog.setMessage("Do you want to save photo?")
            dialog.setPositiveButton("Yes",{ dialogInterface: DialogInterface, i: Int ->
                imageUri = data.data
//            Picasso.get().load(imageUri).transform(ExifTransformation(context!!)).into(imageView)
                Glide
                    .with(context!!)
                    .load(imageUri)
                    .into(imageView)
            })
            dialog.setNegativeButton("No",{ dialogInterface: DialogInterface, i: Int ->

            })
            dialog.show()


        }
    }

    private fun signOut() {
        mGoogleSignInClient!!.signOut()
            .addOnCompleteListener(activity!!, object : OnCompleteListener<Void> {
                override fun onComplete(task: Task<Void>) {
                }
            })
    }

    private fun fetchAccount() {
        val jsonObjectRequestprof = object : JsonObjectRequest(
            Method.GET, url_account, null,
            Response.Listener<JSONObject> { response ->

                if (response.getInt("status_code")==200) {
                    val data = response.getJSONObject("data")
                    val email = data.getString("email")
                    val phone = data.getString("phone")

                    emailview!!.text = email
                    phoneview!!.text = phone

                    val bundle_email = Bundle()
                    val bundle_phone = Bundle()
                    bundle_email.putString("email",email)
                    bundle_phone.putString("phone",phone)

                    view!!.phone_account.setOnClickListener{
                        view!!.findNavController().navigate(R.id.action_accountFragment_to_phoneFragment, bundle_phone)
                    }
                    view!!.phonenumber_account.setOnClickListener{
                        view!!.findNavController().navigate(R.id.action_accountFragment_to_phoneFragment, bundle_phone)
                    }
                    view!!.email_account.setOnClickListener{
                        view!!.findNavController().navigate(R.id.action_accountFragment_to_emailFragment, bundle_email)
                    }
                    view!!.emailaddress_account.setOnClickListener{
                        view!!.findNavController().navigate(R.id.action_accountFragment_to_emailFragment, bundle_email)
                    }

                } else {

                }


            },
            Response.ErrorListener {
                Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequestprof.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequestprof)

        val jsonObjectRequestacc = object : JsonObjectRequest(
            Request.Method.GET, url_profile, null,
            Response.Listener<JSONObject> { response ->
                if (response.getInt("status_code")==200) {
                    val data = response.getJSONObject("data")
                    val name = data.getString("full_name")
                    val birthday = data.getString("birthday")
                    val gender = data.getString("gender")

                    nameview!!.text = name
                    birthview!!.text = birthday
                    genderview!!.text = gender
                } else {

                }

                    //val total = totaltext!!.toInt()

            },
            Response.ErrorListener {
                Toast.makeText(activity, "Error", Toast.LENGTH_LONG).show()
            })
        {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["authorization"] = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjFiNDMxOWU2OThmOGQyMzM2OTBlZGZmM2JhOTk0YjBhOTM0YzQ3NWI5Y2M2OWMwNGI5YWQyMmVlYWI3Zjg0ZDQ4OWIxN2QzYjRhYTYwOGVlIn0.eyJhdWQiOiIxIiwianRpIjoiMWI0MzE5ZTY5OGY4ZDIzMzY5MGVkZmYzYmE5OTRiMGE5MzRjNDc1YjljYzY5YzA0YjlhZDIyZWVhYjdmODRkNDg5YjE3ZDNiNGFhNjA4ZWUiLCJpYXQiOjE1NjE3MTE3NzcsIm5iZiI6MTU2MTcxMTc3NywiZXhwIjoxNTkzMzM0MTc3LCJzdWIiOiI0NCIsInNjb3BlcyI6W119.GyvovSCMcsKPFzkUMFWYid2aqGIFfcMQ5IuK1ABpZ1_g-pfG2_umpFWVBq2vflnQrXoQz0pENbEG4GMjqUDyHlYyJ--49Qk63nzc00VBzL5KuYx7FhIW2PNfrE9luk3aD42yJuDMVhHvqflplqPAfQpSQjyyUuxgKZYyyS9_wysmzik7BIeTshyyIn8HpWRwMnxyHWT5z0AqkEVTqAd88Ev3WcG25Qo5321QhIV0QaO7XWAg0Kv0heQSXQcmVNwEKcQHgdHgJ2jGwtDDQ5WDGAJu23MixoM5N4BzqTlkO1lIH1-I30M4kvYT48PTtcIc9LMK69vtVzAx1S7m1EqVTMrQltwHw--EBUgrimkPZhkx_glJsmJNmw5QshiR6ytE4eg9tWmc9m0k0kvczzC3TtMUcK3fuOdWgaqB1xnEd2a2Hjb5S_7M6XqYKPbwCooQcrF5F57sGLo9OGJrj-YLE7LMYciDFTQ47oxRGj-C9xNsQRGqpYGCTntSsYlcFltHehpjG1Sdkc6beNdb7cJN9ksEgwXs2--TKw4h8GWVL1mTvE1PP3iQWr9HOJ9rebCSkWOqaVuLzGCT3O_kF6o6WzBWtFnPEOxm9sRl4JOrbj7Yh6v8i0TnQAXIjTAOvoACE9Nwitl8f0BU9lwI3uWegbcz3qmfvNjJaYoBQGkuBf4"
                return headers
            }
        }
        jsonObjectRequestacc.retryPolicy = DefaultRetryPolicy(
            7500,
            // 0 means no retry
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES, // DefaultRetryPolicy.DEFAULT_MAX_RETRIES = 2
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT // DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue!!.add(jsonObjectRequestacc)

    }


}
